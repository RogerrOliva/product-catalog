package com.reavature.productcatalog

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer

import androidx.navigation.fragment.findNavController
import com.reavature.productcatalog.databinding.FragmentLoginBinding
import com.reavature.productcatalog.vendor.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null

    private val binding: FragmentLoginBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val myActivity: MainActivity = activity as MainActivity

        val loginViewModel: LoginViewModel by viewModels()

        // !!! OBSERVER !!! //
        //======================================================================================================//
        val loginResultObserver = Observer<LoginViewModel.LoginResult> { loginResult ->

            if (loginResult.currentLoginStatus == LoginViewModel.LoginResult.loginStatusFailed) {
                et_email.error = "Please Enter Correct Username"
                et_password.error = "Please Enter Correct Password"

            } else if (loginResult.currentLoginStatus == LoginViewModel.LoginResult.loginStatusSuccess) {
                myActivity.setAddItemsButtonStatus()
                if (binding.switchForOwnerMode.isChecked) {
                    findNavController().navigate(R.id.action_global_mainMenuFragment)
                } else {
                    findNavController().navigate(R.id.action_global_buyerMainMenuFragment)
                }
            }
        }

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        loginViewModel.loginResultLiveData.observe(viewLifecycleOwner, loginResultObserver)
        //======================================================================================================//


        _binding = FragmentLoginBinding.inflate(inflater, container, false)

        binding.tapForRegisterButton.setOnClickListener {
            findNavController().navigate(R.id.action_global_choiceRegisterFragment)
        }

        binding.tapForRegisterTextView.setOnClickListener {
            findNavController().navigate(R.id.action_global_choiceRegisterFragment)
        }

        binding.btnLogin.setOnClickListener {
            /**
             * Added navigation here to skip Login request. This is only to show case new catalog changes that are dependent on Apiary.
             * I also commented out ViewModel Login request for the same reason. Button will automatically take you to MainMenu
             */
//            findNavController().navigate(R.id.action_global_mainMenuFragment)
            if (binding.switchForOwnerMode.isChecked){
                loginViewModel.loginRequest(binding.etEmail.text.toString(), binding.etPassword.text.toString())
            } else {
                loginViewModel.loginBuyerRequest(binding.etEmail.text.toString(), binding.etPassword.text.toString())
            }
            myActivity.isOwner = binding.switchForOwnerMode.isChecked
        }

        return binding.root
    }

}

