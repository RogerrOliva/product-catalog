package com.reavature.productcatalog.authmodel

data class LoginResponse(
    var token: String
)

data class User(
    var username: String,
    var token:String

)

object UserDatabase {

    var currentUser: User? = null
    var userList: MutableMap<String,User> = mutableMapOf()


    fun connectTokenWithCurrentUser(username:String, token:String):Boolean {
        userList[username] = User(username,token)
        return true
    }
}

