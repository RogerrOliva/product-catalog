package com.reavature.productcatalog.authmodel

data class BuyerOrderResponse(
    val order_id : Int,
    val product_name : String,
    val price : Double,
    val vendor_name : String
)
