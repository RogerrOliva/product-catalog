package com.reavature.productcatalog.customer.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.reavature.productcatalog.authmodel.BuyerDatabase
import com.reavature.productcatalog.authmodel.BuyerProfileResponse
import com.reavature.productcatalog.network.RetrofitService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class UpdateBuyerProfileViewModel: ViewModel() {

    var updateAbility = false

    val buyerStatusLiveData: MutableLiveData<BuyerLoadingStatus> by lazy {
        MutableLiveData<BuyerLoadingStatus>()
    }

    fun profileRequest() {

        val jsonObject = JSONObject()

        jsonObject.put("id", BuyerDatabase.currentUser?.cust_id)

//        Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//        Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.getBuyerProfile(requestBody)
            val buyerLoadingStatus = BuyerLoadingStatus()

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    response.body()?.let { responseBuyerData ->
                        buyerLoadingStatus.currentBuyerStatus = BuyerLoadingStatus.buyerStatusSuccess
                        buyerLoadingStatus.currentBuyerData = responseBuyerData
                        buyerStatusLiveData.setValue(buyerLoadingStatus)
                    }
                    Log.e("ResponseBody", "${response.body()}")
                }
            }
        }
    }

    fun updateUserRequest(
        username: String,
        password: String,
        email: String,
        name: String,
        phone: String,
        address: String
    ) {

        val jsonObject = JSONObject()

        val cust_id = BuyerDatabase.currentUser?.cust_id

        jsonObject.put("cust_id", cust_id)
        jsonObject.put("username", username)
        jsonObject.put("password", password)
        jsonObject.put("email", email)
        jsonObject.put("name", name)
        jsonObject.put("phone", phone)
        jsonObject.put("address", address)

//        Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//        Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.updateBuyer(requestBody)
            val updateBuyerResult = BuyerLoadingStatus()
            val errorMessage =  "Could not update buyer."

            withContext(Dispatchers.Main) {

                if (response.isSuccessful) {
                    Log.e("RETROFIT REQUEST", "${response.code()} + ${response.body()?.message}")

                    if (response.body()?.message != null || response.body()?.message != errorMessage) {
                        updateBuyerResult.currentBuyerStatus = BuyerLoadingStatus.buyerStatusSuccess
                        updateBuyerResult.currentBuyerData = response.body()
                        buyerStatusLiveData.value = updateBuyerResult

                        Log.e("RETROFIT_SUCCESS", response.code().toString())
                    }
                } else {
                    updateBuyerResult.currentBuyerStatus = BuyerLoadingStatus.buyerStatusFailed
                    buyerStatusLiveData.value = updateBuyerResult
                    Log.e("RETROFIT_ERROR", response.code().toString())
                }
            }
        }
    }


    class BuyerLoadingStatus {

        var currentBuyerStatus = buyerStatusUnknown
        var currentBuyerData: BuyerProfileResponse? = null

        companion object {
            val buyerStatusSuccess = "buyerStatusSuccess"
            val buyerStatusFailed = "buyerStatusFailed"
            val buyerStatusUnknown = "buyerStatusUnknown"
        }
    }
}