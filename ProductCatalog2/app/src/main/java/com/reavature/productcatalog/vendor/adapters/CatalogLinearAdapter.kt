package com.reavature.productcatalog.vendor.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.contentValuesOf
import androidx.lifecycle.LiveData
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.catalogmodel.CatalogResponse
import com.reavature.productcatalog.network.RetrofitService
import com.reavature.productcatalog.vendor.fragments.VendorCatalogFragment
import com.reavature.productcatalog.vendor.fragments.viewPagerContainerDirections
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_add_item.view.*
import kotlinx.android.synthetic.main.vendor_catalog_list_item.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Response
import java.security.AccessController.getContext
import java.util.*
import kotlin.collections.ArrayList

class CatalogLinearAdapter(private val catalogList: List<CatalogResponse>, val context: Context) :
    RecyclerView.Adapter<CatalogLinearAdapter.CatalogViewHolder>(), Filterable {

    private var catalogListFiltered = catalogList

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CatalogLinearAdapter.CatalogViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.vendor_catalog_linear_list_item, parent, false)
        return CatalogViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CatalogLinearAdapter.CatalogViewHolder, position: Int) {

        val productID = catalogListFiltered[position]
        val prodID = productID.id
        val catalogData = catalogListFiltered[position]
        if (catalogData.img0 != "") {
            Picasso.get().load(catalogData.img0).into(holder.productImg0)
//                Picasso.get().load(catalogData.picture).into(holder.productImg1)
//                Picasso.get().load(catalogData.picture).into(holder.productImg3)
        }

        holder.productName.text = catalogData.item

        holder.itemView.btn_delete.setOnClickListener {
            Log.e("Trash Icon", "Delete")
            showAlertDialog(prodID)
        }
        holder.productPrice.text = "$${catalogData.selling_price}"



            holder.itemView.btn_more_details.setOnClickListener {
                val jsonObject = JSONObject()
                jsonObject.put("id", prodID)
//             Convert JSONObject to String
                val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
                val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

                CoroutineScope(Dispatchers.IO).launch {
                    // Do the POST request and get response

//            ,"Bearer ${UserDatabase.currentUser?.token.toString()}
                    val response = RetrofitService.instance.getProduct(
                        requestBody,
                        "Bearer ${UserDatabase.currentUser?.token.toString()}"
                    )

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            Log.e("PRODUCT_REQUEST_SUCCESS", "Single Product")
                            val action = response.body()?.selling_price?.let { sellingPrice ->
                                response.body()?.actual_price?.let { actualPrice ->
                                    response.body()?.quantity?.let { quantity ->
                                        viewPagerContainerDirections.actionViewPagerContainerFragmentToProductDetailFragment(
                                            response.body()?.item.toString(),
                                            sellingPrice.toString().toFloat(),
                                            actualPrice.toString().toFloat(),
                                            quantity.toString().toFloat(),
                                            response.body()?.unit.toString(),
                                            response.body()?.variants.toString(),
                                            response.body()?.description.toString(),
                                            response.body()?.img0.toString(),
                                            response.body()?.img1.toString(),
                                            response.body()?.img2.toString(),
                                            response.body()?.id.toString().toInt()
                                        )
                                    }
                                }
                            }
                            if (action != null) {
                                Log.e("response",response.body().toString())
                                holder.itemView.findNavController().navigate(action)
                            }
                        } else {
                            Log.e("PRODUCT_REQUEST_FAILED", "Single Product")
                            showFailedProductDialog()
                        }
                        if (response.body() == null) {
                            Log.e("PRODUCT_REQUEST_FAILED", "SIngle Product")
                            showFailedProductDialog()

                        }
                    }
                }
            }
    }

    override fun getItemCount(): Int {
        return catalogListFiltered.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val searchString = constraint.toString()
                catalogListFiltered = if (searchString.isEmpty()) {
                    catalogList
                } else {
                    val filterList = ArrayList<CatalogResponse>()
                    catalogListFiltered.forEach { row ->
                        if (row.item.toLowerCase(Locale.ROOT).trim()
                                .contains(searchString.toLowerCase(Locale.ROOT))
                        ) {//if // the last name contains the string
                            filterList.add(row)
                        }

                    }

                    filterList
                }

                val filterResults = FilterResults()
                filterResults.values = catalogListFiltered
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results?.values is ArrayList<*>) {
                    catalogListFiltered = results.values as ArrayList<CatalogResponse>
                    notifyDataSetChanged()
                }
            }
        }
    }


    inner class CatalogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var productImg0: ImageView = itemView.findViewById(R.id.img_product0)

        //        var productImg1: ImageView = itemView.findViewById(R.id.img_product1)
//        var productImg3: ImageView = itemView.findViewById(R.id.img_product3)
        var productName: TextView = itemView.findViewById(R.id.tv_product_name)
        var productPrice: TextView = itemView.findViewById(R.id.tv_product_price)
//        var productUnit: TextView = itemView.findViewById(R.id.tv_product_unit)
//        var productDesc: TextView = itemView.findViewById(R.id.tv_product_description)


    }

    private fun showAlertDialog(id:Int) {

        MaterialAlertDialogBuilder(context)
            .setTitle("Alert")
            .setMessage("You are deleting product. You can't undue this action. Do you wish to continue?")
            .setNeutralButton("Cancel") { dialog, Which ->
                Log.e("Cancel", "cancel")
            }
            .setNegativeButton("No") { dialog, Which ->
                Log.e("No", "no")
            }
            .setPositiveButton("yes") { dialog, Which ->
                Log.e("Yes", "Call Delete API")
                deleteProductRequest(id)
            }

            .show()

    }

    private fun showSucessDeleteDialog() {

        MaterialAlertDialogBuilder(context)
            .setTitle("Deletion Successful")
            .setMessage("Product has been successfully delete")

            .setPositiveButton("Thank you") { dialog, Which ->
                Log.e("Yes", "Call Delete API")
            }

            .show()

    }

    private fun deleteProductRequest(id:Int) {

        val jsonObject = JSONObject()
        jsonObject.put("id", id)
//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response

//            ,"Bearer ${UserDatabase.currentUser?.token.toString()}
            val response = RetrofitService.instance.removeProduct(
                requestBody,
                "Bearer ${UserDatabase.currentUser?.token.toString()}"
            )

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Log.e("DELETE_REQUEST_SUCCESS","delete")
                    showSucessDeleteDialog()
                } else {
                    Log.e("DELETE_REQUEST_FAILE","notdelete")
                }
                if(response.body() == null) {
                    Log.e("DELETE_REQUEST_FAILE","notdelete")

                }
            }
        }
    }

    private fun showFailedProductDialog() {

        MaterialAlertDialogBuilder(context)
            .setTitle("Could Not Get Produce")
            .setMessage("Sorry something went wrong, please try again")

            .setPositiveButton("Okay") { dialog, Which ->
                Log.e("okay", "Product Detail dialog")
            }

            .show()

    }



}