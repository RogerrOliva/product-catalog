package com.reavature.productcatalog.aws

import android.content.Context
import android.net.ParseException
import android.text.TextUtils
import android.util.Log.e
import com.amazonaws.HttpMethod
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.logging.Log
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest
import com.amazonaws.services.s3.model.ResponseHeaderOverrides
import com.reavature.productcatalog.authmodel.ImageDatabase
import java.io.File
import java.lang.Exception
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class AWSUtils(private val context: Context, private val filePath:String, val onAwsImageUploadListener: OnAwsImageUploadListener, val filePathKey:String) {

    private var image: File? = null
    private var mTransferUtility: TransferUtility? = null

    private var sS3CLient: AmazonS3Client? = null
    private var sCredProvider: CognitoCachingCredentialsProvider? = null

    private fun getCredProvider(context:Context):CognitoCachingCredentialsProvider? {
        if(sCredProvider == null){
            sCredProvider = CognitoCachingCredentialsProvider(context.applicationContext, AwsConstants.COGNITO_IDENTITY_ID, AwsConstants.COGNITO_REGION)
        }
        return sCredProvider
    }

    private fun getS3Client(context: Context):AmazonS3Client?{
        if(sS3CLient == null){
            sS3CLient = AmazonS3Client(getCredProvider(context))
            sS3CLient!!.setRegion(Region.getRegion(Regions.US_EAST_2))
        }

        return sS3CLient
    }

    private fun getTransferUtility(context:Context):TransferUtility?{
        if(mTransferUtility == null){
            mTransferUtility = TransferUtility(
                getS3Client(context.applicationContext),
                context.applicationContext
            )
        }
        return mTransferUtility
    }

    fun beginUpload(){
        if(TextUtils.isEmpty(filePath)){
            onAwsImageUploadListener.onError("Could not find the filepath of the selected image")
        }

        onAwsImageUploadListener.showProgressDialog()
        val file = File(filePath)
        image = file

        try{
            val observer = getTransferUtility(context)?.upload(AwsConstants.BUCKET_NAME, filePathKey + file.name, image)
            observer?.setTransferListener(UploadListener())
        }
        catch (e:Exception){
            e.printStackTrace()
            onAwsImageUploadListener.hideProgressDialog()
        }
    }

    fun generateS3SignedUrl(path:String?): String?{
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrow = calendar.time

        val dateFormat:DateFormat = SimpleDateFormat("MM,dd,yyyy", Locale.getDefault())
        val tomorrowAsString = dateFormat.format(tomorrow)

        //maximum 7 days allowed
        val EXPIRY_DATE = tomorrowAsString
        val mFile = File(path)

        val s3Client: AmazonS3Client? = getS3Client(context)

        val expiration = Date()
        var msec:Long = expiration.time
        msec += 1000 * 6000 * 6000.toLong()

        val format:DateFormat = SimpleDateFormat("MM,dd,yyyy", Locale.getDefault())
        val date:Date?

        try{
            date = format.parse(EXPIRY_DATE)
            expiration.time = date.time
        }
        catch (e: ParseException){
            e.printStackTrace()
            expiration.time = msec
        }

        val overrideHeader = ResponseHeaderOverrides()
        overrideHeader.contentType = "image/jpeg"
        val mediaUrl: String = mFile.name

        val generatePreSignedUrlRequest = GeneratePresignedUrlRequest(AwsConstants.BUCKET_NAME, filePathKey + mediaUrl)
        generatePreSignedUrlRequest.method = HttpMethod.GET
        generatePreSignedUrlRequest.expiration = expiration
        generatePreSignedUrlRequest.responseHeaders = overrideHeader

        return s3Client!!.generatePresignedUrl(generatePreSignedUrlRequest).toString()
    }

    private inner class UploadListener : TransferListener{
        override fun onError(id: Int, ex: Exception?) {
            onAwsImageUploadListener.hideProgressDialog()
        }

        override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
        }

        override fun onStateChanged(id: Int, state: TransferState?) {
            if(state == TransferState.COMPLETED){
                onAwsImageUploadListener.hideProgressDialog()

                val finalImageUrl = AwsConstants.S3_URL + filePathKey + image!!.name
                println(" Final Object  $finalImageUrl")
                onAwsImageUploadListener.onSuccess(generateS3SignedUrl(finalImageUrl)!!)

                ImageDatabase.imageList.add(finalImageUrl)

//                ImageDatabase.imageList.add(0,"0")
//                ImageDatabase.imageList.add(1,"1")
//                ImageDatabase.imageList.add(2,"2")
//
//                ImageDatabase.imageList.add(0,finalImageUrl)
//                ImageDatabase.imageList.add(1,finalImageUrl)
//                ImageDatabase.imageList.add(2,finalImageUrl)

//                if(ImageDatabase.imageList[0] != finalImageUrl){
//                    ImageDatabase.imageList[0] = finalImageUrl
//                }
//
//                if(ImageDatabase.imageList[2].contains(finalImageUrl)){
//                    ImageDatabase.imageList[2] = finalImageUrl
//                }

//                if(ImageDatabase.imageList[0].contains("[https://lonlonbucket.s3.us-east-2.amazonaws.com")){
//                    ImageDatabase.imageList.removeAt(0)
//                    ImageDatabase.imageList.add(0,finalImageUrl)
//                }
//
//              if(ImageDatabase.imageList[0].isNotEmpty()) {
//
//                  if (ImageDatabase.imageList[1].contains("[https://lonlonbucket.s3.us-east-2.amazonaws.com")) {
//                      ImageDatabase.imageList.removeAt(1)
//                      ImageDatabase.imageList.add(1, finalImageUrl)
//                  }
//              }
//
//                if(ImageDatabase.imageList[0].isNotEmpty()) {
//
//                    if (ImageDatabase.imageList[2].contains("[https://lonlonbucket.s3.us-east-2.amazonaws.com")) {
//                        ImageDatabase.imageList.removeAt(2)
//                        ImageDatabase.imageList.add(2, finalImageUrl)
//                    }
//                }




                println(ImageDatabase.imageList)

            }
            else if(state == TransferState.CANCELED || state == TransferState.FAILED)
            {
                onAwsImageUploadListener.hideProgressDialog()
                onAwsImageUploadListener.onError("Error in upload file")
            }
        }
    }

    interface OnAwsImageUploadListener{
        fun showProgressDialog()
        fun hideProgressDialog()
        fun onSuccess(imgUrl:String)
        fun onError(errorMsg:String)
    }
}