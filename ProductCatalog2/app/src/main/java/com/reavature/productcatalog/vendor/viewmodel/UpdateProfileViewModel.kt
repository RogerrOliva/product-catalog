package com.reavature.productcatalog.vendor.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.authmodel.VendorProfileResponse
import com.reavature.productcatalog.network.RetrofitService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.util.*

class UpdateProfileViewModel: ViewModel() {

    var updateAbility = false

    val userStatusLiveData: MutableLiveData<UserLoadingStatus> by lazy {
        MutableLiveData<UserLoadingStatus>()
    }

    fun profileRequest() {

        val jsonObject = JSONObject()

//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//             Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.getVendorProfile(requestBody, "Bearer ${UserDatabase.currentUser?.token.toString()}")
            val userLoadingStatus = UserLoadingStatus()

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    response.body()?.let { responseUserData ->
                        userLoadingStatus.currentUserStatus = UserLoadingStatus.userStatusSuccess
                        userLoadingStatus.currentUserData = responseUserData
                        userStatusLiveData.setValue(userLoadingStatus)
                    }
                    Log.e("ResponseBody", "${response.body()}")
                }
            }
        }
    }

    fun updateUserRequest(
        owner: String,
        businessName: String,
        username:String,
        password:String,
        phone:String,
        street:String,
        city:String,
        state:String,
        zipcode:String,
        category:String
    ) {

        val jsonObject = JSONObject()

        jsonObject.put("owner", owner)
        jsonObject.put("name", businessName)
        jsonObject.put("username", username)
        jsonObject.put("password", password)
        jsonObject.put("phone", phone)
        jsonObject.put("street", street)
        jsonObject.put("city", city)
        jsonObject.put("state", state)
        jsonObject.put("zipcode", zipcode)
        jsonObject.put("category", category)

//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//             Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.updateBusiness(requestBody, "Bearer ${UserDatabase.currentUser?.token.toString()}")
            val updateUserResult = UserLoadingStatus()
            val errorMessage =  "Could not update user."

            withContext(Dispatchers.Main) {


                if (response.isSuccessful) {

                    Log.e("RETROFIT REQUEST", "${response.code()} + ${response.body()?.message}")

                    if (response.body()?.message == errorMessage || response.code() >= 400 || requestBody.equals("")) {
                        updateUserResult.currentUserStatus = UserLoadingStatus.userStatusFailed
                        userStatusLiveData.value = updateUserResult
                        Log.e("RETROFIT_ERROR", response.code().toString())

                    } else if (response.body()?.message != null || response.body()?.message != errorMessage) {
                        updateUserResult.currentUserStatus = UserLoadingStatus.userStatusSuccess
                        updateUserResult.currentUserData = response.body()
                        userStatusLiveData.value = updateUserResult
                        Log.e("RETROFIT_SUCCESS", response.code().toString())
                    }
                } else {
                    updateUserResult.currentUserStatus = UserLoadingStatus.userStatusFailed
                    userStatusLiveData.value = updateUserResult
                    Log.e("RETROFIT_ERROR", response.code().toString())
                }
            }
        }
    }

    class UserLoadingStatus {

        var currentUserStatus = userStatusUnknown
        var currentUserData: VendorProfileResponse? = null

        companion object {
            val userStatusSuccess = "userStatusSuccess"
            val userStatusFailed = "userStatusFailed"
            val userStatusUnknown = "userStatusUnknown"
        }
    }
}


