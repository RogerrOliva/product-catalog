package com.reavature.productcatalog.authmodel

data class RemoveItemResponse(
    val message: String
)
