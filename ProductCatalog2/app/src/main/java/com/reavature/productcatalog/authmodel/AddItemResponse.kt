package com.reavature.productcatalog.authmodel



data class AddItemResponse(
    val message:String
)

data class Img(
    var image0: String,
    var image1:String,
    var image2:String
)

object ImageDatabase {

    var imageList: ArrayList<String> = arrayListOf()


    fun addImagefromS3(img:String):Boolean {
       imageList.add(img)
        return true
    }
}