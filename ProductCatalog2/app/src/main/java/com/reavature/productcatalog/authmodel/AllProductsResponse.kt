package com.reavature.productcatalog.authmodel

//{
//    "product_id": 16,
//    "vendor_id": 1,
//    "item": "Sailor Pro Gear Four Seasons",
//    "description": "Pro Gear, Meigetsu, MF nib",
//    "variants": "",
//    "img0": "https://lonlonbucket.s3.us-east-2.amazonaws.com/IMG_20210509_194209.jpg",
//    "img1": "https://lonlonbucket.s3.us-east-2.amazonaws.com/IMG_20210508_141112.jpg",
//    "img2": "https://lonlonbucket.s3.us-east-2.amazonaws.com/IMG_20210508_141118.jpg",
//    "actual_price": 180,
//    "selling_price": 175.99,
//    "quantity": 7,
//    "unit": ""
//},

data class AllProductsResponse(
    val product_id: Int,
    val vendor_id:Int,
    val item: String,
    val description: String,
    val variants: String,
    val img0: String,
    val img1:String,
    val img2:String,
    val actual_price: Double,
    val selling_price: Double,
    val quantity: Double,
    val unit: String
)
