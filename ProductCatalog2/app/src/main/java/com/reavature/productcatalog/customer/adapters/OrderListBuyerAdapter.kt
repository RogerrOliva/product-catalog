package com.reavature.productcatalog.customer.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.BuyerOrderResponse
import com.reavature.productcatalog.vendor.adapters.CatalogLinearAdapter
import kotlinx.android.synthetic.main.order_list_buyer_item.view.*

class OrderListBuyerAdapter (private val orderList: List<BuyerOrderResponse>, val context: Context) :
    RecyclerView.Adapter<OrderListBuyerAdapter.OrderListViewHolder>() {

    class OrderListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)  {
        val orderField = itemView.tv_order_id
        val itemField = itemView.tv_items
        val priceField = itemView.tv_price
        val vendorField = itemView.tv_vendor_name
        @SuppressLint("SetTextI18n")
        fun bind(bOR: BuyerOrderResponse) {
            orderField.text = "Order Id: " + bOR.order_id
            itemField.text = "Item ordered: " + bOR.product_name
            priceField.text = "Price: " + bOR.price.toString()
            vendorField.text = "Vendor name: " + bOR.vendor_name
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.order_list_buyer_item, parent, false)
        return OrderListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: OrderListViewHolder, position: Int) {
        when(holder) {
            is OrderListViewHolder -> {
                holder.bind(orderList.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return orderList.size
    }

    private fun showFailedProductDialog() {
        MaterialAlertDialogBuilder(context)
            .setTitle("Could Not Get Orders")
            .setMessage("Sorry something went wrong, please try again")
            .setPositiveButton("Okay") { dialog, Which ->
                Log.e("okay", "Order Detail dialog")
            }
            .show()
    }
}