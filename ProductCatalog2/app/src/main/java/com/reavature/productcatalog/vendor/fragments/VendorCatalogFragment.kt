package com.reavature.productcatalog.vendor.fragments

import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.reavature.productcatalog.MainActivity
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.catalogmodel.CatalogResponse
import com.reavature.productcatalog.databinding.FragmentVendorcatalogBinding
import com.reavature.productcatalog.network.RetrofitService
import com.reavature.productcatalog.vendor.adapters.CatalogAdapter
import com.reavature.productcatalog.vendor.adapters.CatalogLinearAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.util.*


class VendorCatalogFragment : Fragment(), SearchView.OnQueryTextListener {

    private var _binding: FragmentVendorcatalogBinding? = null

    private val binding: FragmentVendorcatalogBinding
        get() = _binding!!


    private lateinit var vendorRecyclerView: RecyclerView

    private lateinit var recyclerViewAdapter: CatalogAdapter

    private lateinit var catalogData: List<CatalogResponse>

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

//        val myActivity: MainActivity = activity as MainActivity
//
//        myActivity.checkItemInBottomNavigationBar(R.id.menu_item_view_catalog_page)

        _binding = FragmentVendorcatalogBinding.inflate(inflater, container, false)

        binding.floatingActionButton.backgroundTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.white));

        val animation = AnimationUtils.loadAnimation(context, R.anim.fab_explosion_anim).apply {

            duration = 700
            interpolator = AccelerateDecelerateInterpolator()

        }

        binding.floatingActionButton.setOnClickListener {

            binding.floatingActionButton.isVisible = false
            binding.circle.isVisible = true
            binding.circle.startAnimation(animation) {
                //display fragment
                findNavController().navigate(R.id.action_global_addItemFragment)
            }
        }
        vendorRecyclerView = binding.recyclerviewCatalog
        getCatalogList()

        swipeRefreshLayout = binding.reload

        binding.reload.setOnRefreshListener{
            getCatalogList()

            recyclerViewAdapter.notifyDataSetChanged()
            swipeRefreshLayout.isRefreshing = false
        }
        return binding.root
    }


    private fun getCatalogList() {

        val jsonObject = JSONObject()
        jsonObject.put("", "")
//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response

//            ,"Bearer ${UserDatabase.currentUser?.token.toString()}
            val response = RetrofitService.instance.getCatalog(
                requestBody,
                "Bearer ${UserDatabase.currentUser?.token.toString()}"
            )

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    response.body()?.let { catalogResponse ->
                        catalogData = catalogResponse
                        Log.e("ResponseBody", "${response.body()}")
                        val numberOfColumns = 2;
                        vendorRecyclerView.layoutManager = GridLayoutManager(context, numberOfColumns)
                        recyclerViewAdapter = CatalogAdapter(catalogData,requireContext())
                        vendorRecyclerView.adapter = recyclerViewAdapter
                        clickAndDrag()
                        setHasOptionsMenu(true)
                    }
                }
                if (response.body() == null) {
                    vendorRecyclerView.visibility = View.GONE
                    binding.emptyView.visibility = View.VISIBLE
                    binding.icEmpty.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.filter_menu, menu)
        val searchItem = menu.findItem(R.id.search_catalog)
        val searchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener(this)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        recyclerViewAdapter.filter.filter(query)
        return false

    }

    override fun onQueryTextChange(newText: String?): Boolean {
        recyclerViewAdapter.filter.filter(newText)
        return false
    }


    private fun clickAndDrag() {
        val helper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.ANIMATION_TYPE_DRAG or ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.START,
            0
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                val draggedPosition = viewHolder.adapterPosition
                val targetPosition = target.adapterPosition
                Collections.swap(catalogData, draggedPosition, targetPosition)
                recyclerViewAdapter.notifyItemMoved(draggedPosition, targetPosition)

                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

            }

        })

        helper.attachToRecyclerView(vendorRecyclerView)
    }
}