package com.reavature.productcatalog.authmodel


data class VendorProfileResponse(
    //owner -> owner_name
    //name -> business_name
    //username -> business_username
    //password -> business_password
    //phone -> business_phone_number
    //street -> business_address
    //city -> business_city
    //state -> business_state
    //zipcode -> business_zipcode
    //category -> business_category_spinner

    val owner: String,
    val name: String,
    val username: String,
    val password: String,
    val phone: String,
    val street: String,
    val city: String,
    val state: String,
    val zipcode: String,
    val category: String,
    val message:String
)
