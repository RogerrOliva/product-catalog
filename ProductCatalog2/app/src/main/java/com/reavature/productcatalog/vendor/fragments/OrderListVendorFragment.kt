package com.reavature.productcatalog.vendor.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.authmodel.VendorOrderResponse
import com.reavature.productcatalog.databinding.OrderListVendorFragmentBinding
import com.reavature.productcatalog.network.RetrofitService
import com.reavature.productcatalog.vendor.adapters.OrderListVendorAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class OrderListVendorFragment : Fragment() {

    private var _binding: OrderListVendorFragmentBinding? = null

    private val binding: OrderListVendorFragmentBinding
        get() = _binding!!


    private lateinit var orderRecyclerView: RecyclerView

    private lateinit var recyclerViewAdapter: OrderListVendorAdapter

    private lateinit var orderData: List<VendorOrderResponse>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = OrderListVendorFragmentBinding.inflate(inflater, container, false)
        orderRecyclerView = binding.recyclerviewVendorOrders
        getOrders()
        return binding.root
    }

    private fun getOrders() {

        val jsonObject = JSONObject()
        jsonObject.put("", "")
//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response

//            ,"Bearer ${UserDatabase.currentUser?.token.toString()}
            val response = RetrofitService.instance.getOrders(
                requestBody,
                "Bearer ${UserDatabase.currentUser?.token.toString()}"
            )

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    response.body()?.let { VendorOrderResponse ->
                        orderData = VendorOrderResponse
                        Log.e("ResponseBody", "${response.body()}")
                        recyclerViewAdapter = OrderListVendorAdapter(orderData, requireContext())
                        orderRecyclerView.adapter = recyclerViewAdapter
                    }
                }
                if (response.body() == null) {
                    orderRecyclerView.visibility = View.GONE
                    binding.emptyView.visibility = View.VISIBLE
                    binding.icEmpty.visibility = View.VISIBLE
                }
            }
        }
    }
}