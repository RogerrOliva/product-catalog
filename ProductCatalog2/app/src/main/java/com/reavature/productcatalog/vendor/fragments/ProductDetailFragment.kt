package com.reavature.productcatalog.vendor.fragments


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentUris
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.reavature.productcatalog.MainActivity
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.ImageDatabase
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.aws.AWSUtils
import com.reavature.productcatalog.aws.AwsConstants
import com.reavature.productcatalog.databinding.FragmentProductDetailBinding
import com.reavature.productcatalog.network.RetrofitService
import com.reavature.productcatalog.vendor.viewmodel.ProductDetailViewModel
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import java.io.File
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.net.URISyntaxException
import java.text.SimpleDateFormat
import java.util.*

class ProductDetailFragment : Fragment(), AWSUtils.OnAwsImageUploadListener,
    EasyPermissions.PermissionCallbacks,
    EasyPermissions.RationaleCallbacks {



    private var _binding: FragmentProductDetailBinding? = null

    private val binding: FragmentProductDetailBinding
        get() = _binding!!


    private val select_picture: Int = 200
    private val Request_Image_Capture = 1

    private lateinit var image: ImageView
    private lateinit var image2: ImageView
    private lateinit var image3: ImageView

    lateinit var img0: String
    lateinit var img1: String
    lateinit var img2: String


    var productID: Int = 0
    private var imageClicked: Int = 0

    var currentPhotoPath: String = ""

    var imgCheck: Boolean = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val myActivity: MainActivity = activity as MainActivity

        myActivity.showNavigationBottom()

        // Inflate the layout for this fragment
        _binding = FragmentProductDetailBinding.inflate(inflater)

        val args: ProductDetailFragmentArgs by navArgs()


        try {

            binding.etUpdateProductName.setText(args.productName)
            binding.etUpdateProductPrice.setText(args.sellingPrice.toString())
            binding.etUpdateProductDiscountPrice.setText(args.discountPrice.toString())
            binding.etUpdateProductQuantity.setText(args.quantity.toString())
            binding.etUpdateProductUnis.setText(args.units)
            binding.etUpdateProductVariants.setText(args.varience)
            binding.etUpdateProductDescription.setText(args.description)
            productID = args.id

            img0 = args.img0
            img1 = args.img1
            img2 = args.img2


            Picasso.get().load(args.img0).into(binding.imgProduct0)
            Picasso.get().load(args.img1).into(binding.imgProduct1)
            Picasso.get().load(args.img2).into(binding.imgProduct2)


        } catch (e: InvocationTargetException) {

        }


        val productDetailViewModel: ProductDetailViewModel by viewModels()


        val updateProductDataStatusObserver =
            Observer<ProductDetailViewModel.UpdateProductResult> { updateProductDataStatus ->
                if (updateProductDataStatus.currentUpdateProductDataStatus == ProductDetailViewModel.UpdateProductResult.updateProductStatusFailed) {
                    Log.e("UPDATE_REQUEST", "FAILED")
                    showFailedUpdateDialog()
                } else if (updateProductDataStatus.currentUpdateProductDataStatus == ProductDetailViewModel.UpdateProductResult.updateProductStatusSuccess) {
                    Log.e("UPDATE_REQUEST", "SUCCESS")
                    showSucessUpdateDialog()

                }


            }

        productDetailViewModel.updateProductLiveData.observe(
            viewLifecycleOwner,
            updateProductDataStatusObserver
        )


        image = binding.imgProduct0
        image2 = binding.imgProduct1
        image3 = binding.imgProduct2

        imageViewHandler()

        binding.btnUpdate.setOnClickListener {

            when {
                binding.etUpdateProductName.text.toString() == "" -> binding.etUpdateProductName.error =
                    "Product name cannot be empty"
                binding.etUpdateProductPrice.text.toString()
                    .toDoubleOrNull() == null || binding.etUpdateProductPrice.text.toString()
                    .toDouble() <= 0.0 -> binding.etUpdateProductPrice.error = "Price is an valid"
                binding.etUpdateProductDiscountPrice.text.toString()
                    .toDoubleOrNull() == null || binding.etUpdateProductDiscountPrice.text.toString()
                    .toDouble() <= 0.0 -> binding.etUpdateProductDiscountPrice.error =
                    "Discount price is invalid"
                binding.etUpdateProductDescription.text.toString() == "" -> binding.etUpdateProductDescription.error =
                    "Product Description is empty"
                binding.etUpdateProductQuantity.text.toString()
                    .toDoubleOrNull() == null || binding.etUpdateProductQuantity.text.toString()
                    .toDouble() <= 0.0 -> binding.etUpdateProductQuantity.error =
                    "Product Quantity is invalid"

                else -> productDetailViewModel.updateProductAbility = true
            }

            try {

                if (productDetailViewModel.updateProductAbility) {
                    productDetailViewModel.UpdateProductRequest(
                        productID,
                        binding.etUpdateProductName.text.toString(),
                        binding.etUpdateProductDescription.text.toString(),
                        ImageDatabase.imageList[0],
                        ImageDatabase.imageList[1],
                        ImageDatabase.imageList[2],
                        binding.etUpdateProductPrice.text.toString().toDouble(),
                        binding.etUpdateProductDiscountPrice.text.toString().toDouble(),
                        binding.etUpdateProductQuantity.text.toString().toDouble(),
                        binding.etUpdateProductUnis.text.toString(),
                        binding.etUpdateProductVariants.text.toString()
                    )

                    ImageDatabase.imageList.clear()

                }
            } catch (e: IndexOutOfBoundsException) {
                productDetailViewModel.UpdateProductRequest(
                    productID,
                    binding.etUpdateProductName.text.toString(),
                    binding.etUpdateProductDescription.text.toString(),
                    img0,
                    img1,
                    img2,
                    binding.etUpdateProductPrice.text.toString().toDouble(),
                    binding.etUpdateProductDiscountPrice.text.toString().toDouble(),
                    binding.etUpdateProductQuantity.text.toString().toDouble(),
                    binding.etUpdateProductUnis.text.toString(),
                    binding.etUpdateProductVariants.text.toString()
                )

                ImageDatabase.imageList.clear()

            }
        }

    binding.btnDelete.setOnClickListener{
        showAlertDialog(args.id)

    }

    return binding.root
}


private fun showAlertDialog(id: Int) {

    context?.let {
        MaterialAlertDialogBuilder(it)
            .setTitle("Alert")
            .setMessage("You are deleting product. You can't undue this action. Do you wish to continue?")
            .setNeutralButton("Cancel") { _, _ ->
                Log.e("Cancel", "cancel")
            }
            .setNegativeButton("No") { _, _ ->
                Log.e("No", "no")
            }
            .setPositiveButton("yes") { _, _ ->
                Log.e("Yes", "Call Delete API")
                deleteProductRequest(id)

            }
            .show()
    }

}

private fun deleteProductRequest(id: Int) {

    val jsonObject = JSONObject()
    jsonObject.put("id", id)
//             Convert JSONObject to String
    val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
    val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

    CoroutineScope(Dispatchers.IO).launch {
        // Do the POST request and get response

//            ,"Bearer ${UserDatabase.currentUser?.token.toString()}
        val response = RetrofitService.instance.removeProduct(
            requestBody,
            "Bearer ${UserDatabase.currentUser?.token.toString()}"
        )

        withContext(Dispatchers.Main) {
            if (response.isSuccessful) {
                Log.e("DELETE_REQUEST_SUCCESS", "delete")
                showSucessDeleteDialog()
            } else {
                Log.e("DELETE_REQUEST_FAILE", "notdelete")
            }
            if (response.body() == null) {
                Log.e("DELETE_REQUEST_FAILE", "notdelete")

            }
        }
    }
}

private fun selectImage() {

    val items = arrayOf<CharSequence>("Take Photo", "Gallery", "Cancel")

    val builder = AlertDialog.Builder(this.requireContext())

    builder.setTitle("Add Photo")

    builder.setItems(items) { dialog, item ->
        when {
            items[item] == "Take Photo" -> dispatchTakePictureIntent()
            items[item] == "Gallery" -> galleryIntent()
            items[item] == "Cancel" -> dialog.dismiss()
        }
    }
    builder.show()
}

private fun showSucessDeleteDialog() {

    context?.let {
        MaterialAlertDialogBuilder(it)
            .setTitle("Deletion Successful")
            .setMessage("Product has been successfully delete")

            .setPositiveButton("Thank you") { dialog, Which ->
                Log.e("Yes", "Call Delete API")
                findNavController().navigate(R.id.action_global_ViewPagerContainerFragment)
            }
            .show()
    }

}

private fun showSucessUpdateDialog() {

    MaterialAlertDialogBuilder(requireContext())
        .setTitle("Update is successful")
        .setMessage("Product has been successfully Updated")

        .setPositiveButton("Thank you") { _, _ ->

        }
        .show()

}

private fun showFailedUpdateDialog() {

    MaterialAlertDialogBuilder(requireContext())
        .setTitle("Update Failed")
        .setMessage("Product Update Failed, Please Try Again")

        .setPositiveButton("Try Again") { dialog, Which ->

        }
        .show()

}


fun imageViewHandler() {
    image.setOnClickListener {

        if (hasExternalStorageWritePermission()) {
            selectImage()
        } else {
            EasyPermissions.requestPermissions(
                PermissionRequest.Builder(
                    this,
                    1000,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                    .setRationale("requires storage permission")
                    .setPositiveButtonText("Granted")
                    .setNegativeButtonText("Cancel")
                    .build()
            )
        }
        imageClicked = 1
    }

    image2.setOnClickListener {
        if (hasExternalStorageWritePermission()) {
            selectImage()
        } else {
            EasyPermissions.requestPermissions(
                PermissionRequest.Builder(
                    this,
                    1000,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                    .setRationale("requires storage permission")
                    .setPositiveButtonText("Granted")
                    .setNegativeButtonText("Cancel")
                    .build()
            )
        }
        imageClicked = 2
    }

    image3.setOnClickListener {
        if (hasExternalStorageWritePermission()) {
            selectImage()
        } else {
            EasyPermissions.requestPermissions(
                PermissionRequest.Builder(
                    this,
                    1000,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                    .setRationale("requires storage permission")
                    .setPositiveButtonText("Granted")
                    .setNegativeButtonText("Cancel")
                    .build()
            )
        }
        imageClicked = 3

    }

}

private fun galleryIntent() {
    val intent = Intent()
    intent.type = "image/*"
    intent.action = Intent.ACTION_GET_CONTENT

    startActivityForResult(Intent.createChooser(intent, "Select Picture"), select_picture)
}

override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    super.onActivityResult(requestCode, resultCode, data)

    if (resultCode == Activity.RESULT_OK && requestCode == select_picture) {
        val selectedImageUri = data?.data
        val path: String? = getPath(selectedImageUri!!)

        AWSUtils(this.requireContext(), path!!, this, AwsConstants.folderPath).beginUpload()

        if (imageClicked == 1)
            image.setImageURI(selectedImageUri)
        if (imageClicked == 2)
            image2.setImageURI(selectedImageUri)
        if (imageClicked == 3)
            image3.setImageURI(selectedImageUri)
    }

    if (requestCode == Request_Image_Capture && resultCode == Activity.RESULT_OK) {

        val imageBitmap = File(currentPhotoPath)
        val imageToUri = imageBitmap.toPath()

        AWSUtils(
            this.requireContext(),
            imageToUri.toString(),
            this,
            AwsConstants.folderPath
        ).beginUpload()

        if (imageClicked == 1)
            if (imageBitmap.exists())
                image.setImageURI(Uri.fromFile(imageBitmap))
        if (imageClicked == 2)
            if (imageBitmap.exists())
                image2.setImageURI(Uri.fromFile(imageBitmap))
        if (imageClicked == 3)
            if (imageBitmap.exists())
                image3.setImageURI(Uri.fromFile(imageBitmap))
    }
}

private fun dispatchTakePictureIntent() {

    val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

    try {

        val pictureFile: File = createImageFile()

        val photoURI: Uri = FileProvider.getUriForFile(
            this.requireContext(),
            "com.reavature.productcatalog.fileprovider",
            pictureFile
        )
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
        startActivityForResult(cameraIntent, Request_Image_Capture)
        Log.d("Test", currentPhotoPath)
    } catch (e: Exception) {
        Log.d(
            "Test",
            "I don't know any more someone help me please because i don't know what i am doing anymore"
        )
    }

}

@Throws(IOException::class)
private fun createImageFile(): File {

    val timeStamp: String = SimpleDateFormat("MMddyyyyHHmmss").format(Date())
    val pictureFile = "Product_$timeStamp"
    val storageDir: File? =
        this.requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    val imagePath = File.createTempFile(pictureFile, ".jpg", storageDir)

    currentPhotoPath = imagePath.absolutePath
    return imagePath
}

@SuppressLint("NewApi", "Recycle")
@Throws(URISyntaxException::class)
private fun getPath(selectedImageUri: Uri): String? {
    var uri = selectedImageUri
    var selection: String? = null
    var selectionArgs: Array<String>? = null

    if (DocumentsContract.isDocumentUri(this.requireContext(), uri)) {
        when {
            isExternalStorageDocument(uri) -> {
                val docId = DocumentsContract.getDocumentId(uri)
                val split =
                    docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
            }
            isDownloadsDocument(uri) -> {
                try {
                    val id = DocumentsContract.getDocumentId(uri)
                    uri = ContentUris.withAppendedId(
                        Uri.parse("content://dowloads/public_downloads"),
                        java.lang.Long.valueOf(
                            id
                        )
                    )
                } catch (e: NumberFormatException) {
                    return null
                }
            }
            isMediaDocument(uri) -> {
                val docId = DocumentsContract.getDocumentId(uri)
                val split =
                    docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]
                if ("image" == type) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                }
                selection = "_id=?"
                selectionArgs = arrayOf(split[1])
            }
        }

        if ("content".equals(uri.scheme, ignoreCase = true)) {
            val projection = arrayOf(MediaStore.Images.Media.DATA)

            val cursor: Cursor? = this.requireContext().contentResolver.query(
                uri,
                projection,
                selection,
                selectionArgs,
                null
            )

            try {
                if (cursor == null) {
                    Log.d("Cursor", "Cursor is Empty")
                } else {

                    cursor.moveToFirst()
                    val colum_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                    Log.d("Cursor", "Cursor not Empty")
                    return cursor.getString(colum_index)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    return null
}


private fun isExternalStorageDocument(uri: Uri): Boolean {
    return "com.android.externalstorage.documents" == uri.authority
}

private fun isDownloadsDocument(uri: Uri): Boolean {
    return "com.android.providers.downloads.documents" == uri.authority
}

private fun isMediaDocument(uri: Uri): Boolean {
    return "com.android.providers.media.documents" == uri.authority
}

private fun hasExternalStorageWritePermission(): Boolean {
    return EasyPermissions.hasPermissions(
        this.requireContext(),
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
}

override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
}

override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
    //Open gallery to pick image
    galleryIntent()
}

override fun onRationaleDenied(requestCode: Int) {
}

override fun onRationaleAccepted(requestCode: Int) {
}

override fun showProgressDialog() {
    //progressBar.visibility = View.VISIBLE
    //uploadButton.visibility = View.INVISIBLE
}

override fun hideProgressDialog() {
    //progressBar.visibility = View.INVISIBLE
    //uploadButton.visibility = View.VISIBLE
}

override fun onSuccess(imgUrl: String) {
    println("Uploaded File Path URL: $imgUrl")
}

override fun onError(errorMsg: String) {
    println("Uploaded File Path URL Error: $errorMsg")
}

}


