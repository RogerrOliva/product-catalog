package com.reavature.productcatalog.customer.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.reavature.productcatalog.R
import com.reavature.productcatalog.customer.viewmodel.RegisterBuyerViewModel
import com.reavature.productcatalog.databinding.FragmentRegisterBuyerBinding
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class RegisterBuyerFragment : Fragment() {

    private var _binding: FragmentRegisterBuyerBinding? = null

    private val binding: FragmentRegisterBuyerBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        /**
         * Initializing View Model
         */
        val registerBuyerViewModel: RegisterBuyerViewModel by viewModels()

        /**
         * Observer
         */

        val registerBuyerResultObserver =
            Observer<RegisterBuyerViewModel.RegisterBuyerResult> { registerBuyerResult ->
                if (registerBuyerResult.currentRegisterStatus == RegisterBuyerViewModel.RegisterBuyerResult.registerStatusFailed) {
                    Log.e("REGISTER_REQUEST", "Failed Response")
                    showFailedBuyerRegisterDialog()
                } else if (registerBuyerResult.currentRegisterStatus == RegisterBuyerViewModel.RegisterBuyerResult.registerStatusSuccess) {
                    Log.e("REGISTER_REQUEST", "Success Response")
                    showSucessBuyerRegisterDialog()


                }
            }


        /**
         *  Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
         */
        registerBuyerViewModel.registerByerResultLiveData.observe(
            viewLifecycleOwner,
            registerBuyerResultObserver
        )

        // Inflate the layout for this fragment
        _binding = FragmentRegisterBuyerBinding.inflate(inflater, container, false)


        /**
         * On click on btnRegister, API will be called.
         *
         * I will comment this out for now because
         * I replaced the BASE URL thus my recycler view will no longer have data from APIARY.
         * I want to show recycler view progress for 5/5/2021
         */
        binding.btnRegister.setOnClickListener {

            when {
                binding.buyerUsername.editableText.toString() == "" -> binding.buyerUsername.error =
                    "Username can't be empty"
                binding.buyerPassword.editableText.toString() == "" -> binding.buyerPassword.error =
                    "Password can't be empty"
                binding.buyerPasswordReType.editableText.toString() == "" -> binding.buyerPasswordReType.error =
                    "Password can't be empty"
                binding.buyerEmail.editableText.toString() == "" -> binding.buyerEmail.error =
                    "Email can't be empty"
                binding.buyerName.editableText.toString() == "" -> binding.buyerName.error =
                    "Name can't be empty"
                binding.buyerPhoneNumber.editableText.toString() == "" -> binding.buyerPhoneNumber.error =
                    "Phone can't be empty"
                binding.buyerAddress.editableText.toString() == "" -> binding.buyerAddress.error =
                    "Address can't be empty"
                binding.buyerPassword.text.toString() != binding.buyerPasswordReType.text.toString() -> Toast.makeText(
                    context,
                    "Passwords is not the same",
                    Toast.LENGTH_SHORT
                ).show()

                else -> registerBuyerViewModel.registerBuyerAbility = true
            }

            if (registerBuyerViewModel.registerBuyerAbility) {
                registerBuyerViewModel.registerBuyerRequset(
                    binding.buyerUsername.editableText.toString(),
                    binding.buyerPassword.editableText.toString(),
                    binding.buyerEmail.editableText.toString(),
                    binding.buyerName.editableText.toString(),
                    binding.buyerPhoneNumber.editableText.toString(),
                    binding.buyerAddress.editableText.toString()
                )
            }
        }

        binding.tapForLoginButton.setOnClickListener {
            findNavController().navigate(R.id.action_global_loginFragment)
        }

        binding.tapForLoginTextView.setOnClickListener {
            findNavController().navigate(R.id.action_global_loginFragment)
        }
        return binding.root
    }

    private fun showSucessBuyerRegisterDialog() {

        MaterialAlertDialogBuilder(requireContext())
            .setTitle("You're All Set!")
            .setMessage("Check out our available products!")

            .setPositiveButton("Login") { _, _ ->
                findNavController().navigate(R.id.action_global_loginFragment)
            }
            .show()

    }

    private fun showFailedBuyerRegisterDialog() {

        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Oops!")
            .setMessage("Sorry, something went wrong.Please check your inputs or try again at a later time")

            .setPositiveButton("Try Again") { dialog, Which ->

            }
            .show()

    }
}
