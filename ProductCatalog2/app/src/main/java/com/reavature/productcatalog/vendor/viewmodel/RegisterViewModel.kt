package com.reavature.productcatalog.vendor.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.reavature.productcatalog.network.RetrofitService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class RegisterViewModel: ViewModel() {

    var registerAbility = false

    val registerResultLiveData: MutableLiveData<RegisterResult> by lazy {
        MutableLiveData<RegisterResult>()
    }

    fun registerRequest(
        owner: String,
        businessName: String,
        username:String,
        password:String,
        phone:String,
        street:String,
        city:String,
        state:String,
        zipcode:String,
        category:String

        ) {

        val jsonObject = JSONObject()



        jsonObject.put("owner", owner)
        jsonObject.put("name", businessName)
        jsonObject.put("username", username)
        jsonObject.put("password", password)
        jsonObject.put("phone", phone)
        jsonObject.put("street", street)
        jsonObject.put("city", city)
        jsonObject.put("state", state)
        jsonObject.put("zipcode", zipcode)
        jsonObject.put("category", category)

//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//             Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.registerBusiness(requestBody)
            val registerResult = RegisterResult()
            val errorMessage =  "Could not register user."

            withContext(Dispatchers.Main) {


                if (response.isSuccessful) {

                    Log.e("RETROFIT REQUEST", "${response.code()} + ${response.body()?.message}")

                    if (response.body()?.message == errorMessage || response.code() >= 400 || requestBody.equals("")) {
                        registerResult.currentRegisterStatus = RegisterResult.registerStatusFailed
                        registerResultLiveData.value = registerResult
                        Log.e("RETROFIT_ERROR", response.code().toString())

                    } else if (response.body()?.message != null || response.body()?.message != errorMessage) {
                        registerResult.currentRegisterStatus = RegisterResult.registerStatusSuccess
                        registerResultLiveData.value = registerResult
                        Log.e("RETROFIT_SUCCESS", response.code().toString())
                    }
                } else {
                    registerResult.currentRegisterStatus = RegisterResult.registerStatusFailed
                    registerResultLiveData.value = registerResult
                    Log.e("RETROFIT_ERROR", response.code().toString())
                }
            }
        }
    }

    class RegisterResult {

        var currentRegisterStatus = registerStatusUnknown

        companion object {
            val registerStatusSuccess = "registerStatusSuccess"
            val registerStatusFailed = "registerStatusFailed"
            val registerStatusUnknown = "registerStatusUnknown"
        }
    }
}

