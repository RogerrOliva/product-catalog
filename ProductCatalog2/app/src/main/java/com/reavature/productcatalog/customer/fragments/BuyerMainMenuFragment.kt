package com.reavature.productcatalog.customer.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.reavature.productcatalog.MainActivity
import com.reavature.productcatalog.R
import com.reavature.productcatalog.databinding.FragmentBuyerMainMenuBinding


class BuyerMainMenuFragment : Fragment() {


    private var _binding: FragmentBuyerMainMenuBinding? = null

    private val binding: FragmentBuyerMainMenuBinding
        get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val myActivity: MainActivity = activity as MainActivity

        myActivity.uncheckAllItemsInBottomNavigationBar()

        _binding = FragmentBuyerMainMenuBinding.inflate(inflater, container, false)



        binding.btnViewCatalogBuyerMainMenu.setOnClickListener {
            findNavController().navigate(R.id.action_global_CustomerViewPagerContainerFragment)
        }

        binding.btnUpdateProfileInBuyerMainMenu.setOnClickListener {
            findNavController().navigate(R.id.action_global_updateBuyerProfileFragment)
        }

        binding.btnStatisticBuyerMainMenu.setOnClickListener {
            findNavController().navigate(R.id.action_global_BuyerOrderListFragment)
        }

        //TODO: add other button listeners


        return binding.root
    }
}