package com.reavature.productcatalog.vendor.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.reavature.productcatalog.MainActivity
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.catalogmodel.CatalogResponse
import com.reavature.productcatalog.databinding.FragmentMainMenuBinding
import com.reavature.productcatalog.network.RetrofitService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject


class MainMenuFragment : Fragment() {

    private var _binding: FragmentMainMenuBinding? = null

    private val binding: FragmentMainMenuBinding
        get() = _binding!!

    private lateinit var catalogData: List<CatalogResponse>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val myActivity: MainActivity = activity as MainActivity

        myActivity.showNavigationBottom()

        myActivity.uncheckAllItemsInBottomNavigationBar()

        _binding = FragmentMainMenuBinding.inflate(inflater, container, false)

        val animation = AnimationUtils.loadAnimation(context, R.anim.fab_explosion_anim).apply {

            duration = 700
            interpolator = AccelerateDecelerateInterpolator()

        }

        binding.floatingShareButton.setOnClickListener {

            binding.floatingShareButton.isVisible = false
            binding.circle.isVisible = true
            animation.fillAfter = true;

            binding.circle.startAnimation(animation) {
                getCatalogList()
                findNavController().navigate(R.id.action_global_mainMenuFragment)
            }
        }

        binding.btnUpdateProfileInMainMenu.setOnClickListener {
            findNavController().navigate(R.id.action_global_updateProfileFragment)
        }

        binding.btnAddItemsMainMenu.setOnClickListener {
            findNavController().navigate(R.id.action_global_addItemFragment)
        }

        binding.btnViewCatalogMainMenu.setOnClickListener {
            findNavController().navigate(R.id.action_global_ViewPagerContainerFragment)
        }

        binding.btnVendorOrders.setOnClickListener {
            findNavController().navigate(R.id.action_global_VendorOrderListFragment)
        }

       return binding.root
    }

    private fun getCatalogList() {

        val jsonObject = JSONObject()
        jsonObject.put("", "")
//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response

//            ,"Bearer ${UserDatabase.currentUser?.token.toString()}
            val response = RetrofitService.instance.getCatalog(
                requestBody,
                "Bearer ${UserDatabase.currentUser?.token.toString()}"
            )
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    response.body()?.let { catalogResponse ->
                        catalogData = catalogResponse
                        Log.e("ResponseBody", "${response.body()}")
                        val builder = StringBuilder()
                        for(c in catalogData){
                            builder.append(c.img0 + "\n")
                            builder.append("Product Name: " + c.item + "\n")
                            builder.append("Selling Price: " + c.actual_price.toString() + "\n")
                        }
                        val shareInfo = builder.toString()
                        val sendIntent : Intent = Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(Intent.EXTRA_TEXT, shareInfo)
                            type = "image/*"
                            type = "text/plain"
                        }
                        val shareIntent = Intent.createChooser(sendIntent, null)
                        startActivity(shareIntent)
                    }
                } else {
                    Toast.makeText(context, "Failed to retrieve products.", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

}

