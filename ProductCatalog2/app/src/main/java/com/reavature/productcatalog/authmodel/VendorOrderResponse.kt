package com.reavature.productcatalog.authmodel

data class VendorOrderResponse(
    val order_id: Int,
    val product_name: String,
    val price: Double,
    val cust_name: String,
    val cust_email: String,
    val cust_phone : String
)
