package com.reavature.productcatalog

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.google.android.material.behavior.HideBottomViewOnScrollBehavior
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.reavature.productcatalog.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*


class MainActivity : AppCompatActivity() {

    var isOwner = false
    private lateinit var drawerLayout :DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_container) as NavHostFragment
        //  navHostFragment.navController

        //val drawerBinding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_main)
        drawerLayout = binding.root.drawerLayout

        val navController =  findNavController(R.id.nav_host_fragment_container)

        val appConfig = AppBarConfiguration.Builder(setOf(R.id.mainMenuFragment))
            .setDrawerLayout(drawerLayout)
            .build()

        NavigationUI.setupActionBarWithNavController(this, navController, appConfig)
        NavigationUI.setupWithNavController(navView, navController)

        navView.menu.findItem(R.id.nav_home).setOnMenuItemClickListener {
            if (isOwner) {
                navController.navigate(R.id.action_global_mainMenuFragment)
            } else {
                navController.navigate(R.id.action_global_buyerMainMenuFragment)
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
        navView.menu.findItem(R.id.nav_additem).setOnMenuItemClickListener {
            if (isOwner) {
                navController.navigate(R.id.action_global_addItemFragment)
            } else {
                // TODO: make navigation to view cart fragment
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
        navView.menu.findItem(R.id.nav_catalog).setOnMenuItemClickListener {
            if (isOwner) {
                navController.navigate(R.id.action_global_ViewPagerContainerFragment)
            } else {
                navController.navigate(R.id.action_global_CustomerViewPagerContainerFragment)
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
        navView.menu.findItem(R.id.nav_orders).setOnMenuItemClickListener {
            if (isOwner) {
                navController.navigate(R.id.action_global_VendorOrderListFragment)
            } else {
                navController.navigate(R.id.action_global_BuyerOrderListFragment)
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
        navView.menu.findItem(R.id.nav_update).setOnMenuItemClickListener {
            if (isOwner) {
                navController.navigate(R.id.action_global_updateProfileFragment)
            } else {
                navController.navigate(R.id.action_global_updateBuyerProfileFragment)
            }
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        navView.menu.findItem(R.id.nav_Logout).setOnMenuItemClickListener {
            navController.navigate(R.id.action_global_loginFragment)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        val navigationBottom: BottomNavigationView =
            findViewById(R.id.bottom_navigation) as BottomNavigationView

        navigationBottom.uncheckAllItems()

        navigationBottom.setOnNavigationItemSelectedListener { item ->
            // for navigation we need to use global actions !!!



            when (item.itemId) {
                R.id.menu_item_add_items_page -> {
                    // Respond to navigation item 1 click
                    if (isOwner) {
                        navController.navigate(R.id.action_global_addItemFragment)
                    } else {
                        // TODO: make navigation to view cart fragment
                    }
                    true
                }
                R.id.menu_item_view_catalog_page -> {
                    // Respond to navigation item 2 click
                    if (isOwner) {
                        navController.navigate(R.id.action_global_ViewPagerContainerFragment)
                    } else {
                        navController.navigate(R.id.action_global_CustomerViewPagerContainerFragment)
                    }
                    true
                }
                R.id.menu_item_update_profile -> {
                    // Respond to navigation item 3 click
                    if (isOwner) {
                        navController.navigate(R.id.action_global_updateProfileFragment)
                    } else {
                        navController.navigate(R.id.action_global_updateBuyerProfileFragment)
                    }
                    true
                }
                R.id.menu_item_statistics -> {
                    // Respond to navigation item 4 click
                    if (isOwner) {
                        navController.navigate(R.id.action_global_VendorOrderListFragment)
                    } else {
                        navController.navigate(R.id.action_global_BuyerOrderListFragment)
                    }
                    true
                }
                R.id.menu_item_login_page -> {
                    // Respond to navigation item 5 click
                    navController.navigate(R.id.action_global_loginFragment)
                    true
                }
                else -> false
            }
        }

//        bottom_navigation.setupWithNavController(navHostFragment.navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            bottom_navigation.visibility =
                if (destination.id == R.id.loginFragment ||
                    destination.id == R.id.registerFragment ||
                    destination.id == R.id.registerBuyerFragment ||
                    destination.id == R.id.choiceRegistrationFragment) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
        }

        navController.addOnDestinationChangedListener{ _, destination, _ ->
            if(destination.id == R.id.loginFragment || destination.id == R.id.registerFragment)
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            else
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        }
    }

    fun BottomNavigationView.uncheckAllItems() {
        menu.setGroupCheckable(0, true, false)
        (0..menu.size() - 1).forEach { menu.getItem(it).isChecked = false }
        menu.setGroupCheckable(0, true, true)
    }



    fun uncheckAllItemsInBottomNavigationBar() {
        val navigationBottom: BottomNavigationView =
            findViewById(R.id.bottom_navigation)
        navigationBottom.uncheckAllItems()
    }

    fun setAddItemsButtonStatus() {
        val navigationBottom: BottomNavigationView =
            findViewById(R.id.bottom_navigation)
        navView.menu.findItem(R.id.nav_additem).setVisible(isOwner)
        if (isOwner) {
            navigationBottom.showAddItemsButton()
        } else {
            navigationBottom.hideAddItemsButton()
        }
    }

    fun BottomNavigationView.hideAddItemsButton() {
        menu.findItem(R.id.menu_item_add_items_page).setVisible(false)
    }

    fun BottomNavigationView.showAddItemsButton() {
        menu.findItem(R.id.menu_item_add_items_page).setVisible(true)
    }

    fun checkItemInBottomNavigationBar(itemId: Int) {

        val navigationBottom: BottomNavigationView =
            findViewById<BottomNavigationView>(R.id.bottom_navigation)

        navigationBottom.menu.findItem(itemId).isChecked = true
    }


    fun showNavigationBottom() {

        val navigationBottom: BottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)

        val params: CoordinatorLayout.LayoutParams = navigationBottom.layoutParams as CoordinatorLayout.LayoutParams

        val behavior: HideBottomViewOnScrollBehavior<BottomNavigationView> = params.behavior as HideBottomViewOnScrollBehavior<BottomNavigationView>

        behavior.slideUp(navigationBottom)
    }

    override fun onSupportNavigateUp(): Boolean {
        val appConfig = AppBarConfiguration.Builder(setOf(R.id.nav_home, R.id.mainMenuFragment))
            .setDrawerLayout(drawerLayout)
            .build()
        val navController = this.findNavController(R.id.nav_host_fragment_container)
        return NavigationUI.navigateUp(navController,appConfig)
    }
}