package com.reavature.productcatalog.authmodel

import android.view.inspector.IntFlagMapping

//{
//    "id": 29,
//    "item": "Chocolate Frappucino",
//    "description": "coffee",
//    "variants": "coffee",
//    "img0": "https://lonlonbucket.s3.us-east-2.amazonaws.com/IMG_20210509_194219.jpg",
//    "img1": "https://lonlonbucket.s3.us-east-2.amazonaws.com/IMG_20210509_194209.jpg",
//    "img2": "https://lonlonbucket.s3.us-east-2.amazonaws.com/IMG_20210509_194148.jpg",
//    "actual_price": 10.99,
//    "selling_price": 9.99,
//    "quantity": 1,
//    "unit": ""
//}

data class ProductResponse(
    val id:Int,
    val item:String,
    val variants:String,
    val description:String,
    val img0:String,
    val img1:String,
    val img2:String,
    val actual_price:Double,
    val selling_price:Double,
    val quantity:Double,
    val unit:String,
    val message:String
)
