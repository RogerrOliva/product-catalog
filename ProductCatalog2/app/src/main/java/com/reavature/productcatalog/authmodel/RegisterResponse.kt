package com.reavature.productcatalog.authmodel

//{
//    "owner": "Bob Hastings",
//    "name": "Bob's Shop",
//    "username": "bobshop123",
//    "password": "password123",
//    "phone": "1112223333",
//    "street": "123 Bob Street",
//    "city": "Bobsville",
//    "state": "CA",
//    "zipcode": "90210",
//    "catagory": "Clothing"
//}


data class RegisterResponse(
    val owner: String,
    val name: String,
    val username: String,
    val password: String,
    val phone: String,
    val street: String,
    val city: String,
    val state: String,
    val zipcode: String,
    val category: String,
    val message:String
)
