package com.reavature.productcatalog.vendor.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.VendorOrderResponse
import kotlinx.android.synthetic.main.order_list_buyer_item.view.tv_items
import kotlinx.android.synthetic.main.order_list_buyer_item.view.tv_order_id
import kotlinx.android.synthetic.main.order_list_buyer_item.view.tv_price
import kotlinx.android.synthetic.main.order_list_vendor_item.view.*

class OrderListVendorAdapter (private val orderList: List<VendorOrderResponse>, val context: Context) :
    RecyclerView.Adapter<OrderListVendorAdapter.OrderListViewHolder>() {

    class OrderListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val orderField = itemView.tv_order_id
        val itemField = itemView.tv_items
        val priceField = itemView.tv_price
        val custNameField = itemView.tv_buyer_name
        val custEmailField = itemView.tv_buyer_email
        val custPhoneField = itemView.tv_buyer_phone

        @SuppressLint("SetTextI18n")
        fun bind(vOR: VendorOrderResponse) {
            orderField.text = "Order Id: " + vOR.order_id
            itemField.text = "Item ordered: " + vOR.product_name
            priceField.text = "Price: " + vOR.price.toString()
            custNameField.text = "Buyer name: " + vOR.cust_name
            custEmailField.text = "Buyer e-mail: " + vOR.cust_email
            custPhoneField.text = "Buyer phone: " + vOR.cust_phone
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.order_list_vendor_item, parent, false)
        return OrderListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: OrderListViewHolder, position: Int) {
        when (holder) {
            is OrderListViewHolder -> {
                holder.bind(orderList.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return orderList.size
    }

    private fun showFailedProductDialog() {
        MaterialAlertDialogBuilder(context)
            .setTitle("Could Not Get Orders")
            .setMessage("Sorry something went wrong, please try again")
            .setPositiveButton("Okay") { dialog, Which ->
                Log.e("okay", "Order Detail dialog")
            }
            .show()
    }
}