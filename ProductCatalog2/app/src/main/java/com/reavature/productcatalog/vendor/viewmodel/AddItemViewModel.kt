package com.reavature.productcatalog.vendor.viewmodel

import android.app.Application
import android.app.PendingIntent.getActivity
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.authmodel.VendorProfileResponse
import com.reavature.productcatalog.network.RetrofitService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.security.AccessController.getContext
import java.util.*

class AddItemViewModel(application: Application): AndroidViewModel(application) {

    private val context = getApplication<Application>().applicationContext

    var addItemAbility = false
    val addItemResultLiveData: MutableLiveData<AddItemResult> by lazy {
        MutableLiveData<AddItemResult>()
    }

    fun addItemRequest(
        item: String,
        description: String,
        variants:String,
        img0:String,
        img1:String,
        img2:String,
        actualPrice:Double,
        sellingPrince:Double,
        quantity:Double,
        units:String
        ) {

        val jsonObject = JSONObject()

        /**
         * {
        "item":"Sample",
        "description":"sample description",
        "variants":"blue,red",
        "img0":"https://www.deanfoods.com/wp-content/uploads/2017/12/purity-dp_gal_2rf-357x640.png",
        "img1":"https://schachtgroves.com/wp-content/uploads/2020/10/1_0002_valencia.jpg",
        "img2":"https://i0.wp.com/stdades.com/wp-content/uploads/2018/04/shop-1-img-8.png?resize=600%2C600&ssl=1",
        "actual_price":"10",
        "selling_price":"5",
        "quantity":1,
        "unit":"3kg"
        }
         */

        jsonObject.put("item", item)
        jsonObject.put("description", description)
        jsonObject.put("variants", variants)
        jsonObject.put("img0", img0)
        jsonObject.put("img1", img1)
        jsonObject.put("img2", img2)
        jsonObject.put("actual_price", actualPrice)
        jsonObject.put("selling_price", sellingPrince)
        jsonObject.put("quantity", quantity)
        jsonObject.put("unit", units)

//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.addProduct(requestBody, "Bearer ${UserDatabase.currentUser?.token}")
            val addItemResult = AddItemResult()
            val errorMessage = "Failure to add product."

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    if (response.body()?.message == errorMessage || requestBody.equals("")) {
                        addItemResult.currentAddItemStatus = AddItemResult.addItemStatusFailed
                        addItemResultLiveData.value = addItemResult
                        Log.e("RETROFIT_ERROR", response.code().toString())
                    } else if (response.body()?.message != errorMessage) {
                        Log.e("RETROFIT REQUEST", "${response.code()} + ${response.body()?.message}")
                        addItemResult.currentAddItemStatus = AddItemResult.addItemStatusSuccess
                        addItemResultLiveData.value = addItemResult
                    }

                } else {
                    addItemResult.currentAddItemStatus = AddItemResult.addItemStatusFailed
                    addItemResultLiveData.value = addItemResult
                    Log.e("RETROFIT_ERROR", response.code().toString())
                }
            }
        }
    }

//     fun failedShowAlertDialog(){
//
//        MaterialAlertDialogBuilder(context)
//            .setTitle("Alert")
//            .setMessage("Something went wrong, please check your inputs or try again at a later time")
//            .setPositiveButton("Okay"){dialog, Which ->
//                Log.e("Okau","Okay")
//            }
//            .show()
//
//    }
//
//    fun successShowAlertDialog(){
//
//        MaterialAlertDialogBuilder(context)
//            .setTitle("Success")
//            .setMessage("You Have Added a New Product!")
//            .setPositiveButton("Okay"){dialog, Which ->
//                Log.e("Okay","Okay")
//            }
//            .show()
//
//    }

    class AddItemResult {

        var currentAddItemStatus = addItemStatusUnknown

        companion object {
            val addItemStatusSuccess = "addItemStatusSuccess"
            val addItemStatusFailed = "addItemStatusFailed"
            val addItemStatusUnknown = "addItemStatusUnknown"


        }
    }
}


