package com.reavature.productcatalog.vendor.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.BuyerDatabase
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.network.RetrofitService
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class LoginViewModel : ViewModel() {

    val loginResultLiveData: MutableLiveData<LoginResult> by lazy {
        MutableLiveData<LoginResult>()
    }

    fun loginRequest(username: String, password: String) {

        val jsonObject = JSONObject()

        jsonObject.put("username", username)
        jsonObject.put("password", password)

//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.loginUser(requestBody)
            val loginResult = LoginResult()

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Log.e("RETROFIT REQUEST", "${response.code()} + ${response.body()?.token}")


                    if (response.body()?.token == null || username.isEmpty() || username.isBlank()) {
                        loginResult.currentLoginStatus = LoginResult.loginStatusFailed
                        loginResultLiveData.value = loginResult
                        Log.e("RETROFIT_ERROR", response.code().toString())
                    } else if (response.body()?.token != null) {

                        UserDatabase.connectTokenWithCurrentUser(
                            username,
                            response.body()?.token.toString()
                        )
                        UserDatabase.currentUser = UserDatabase.userList[username]
                        UserDatabase.currentUser?.username = username
                        UserDatabase.currentUser?.token = response.body()?.token.toString()


                        Log.e("Member List", "${UserDatabase.userList}")
                        Log.e("Current Member:", "${UserDatabase.currentUser}")

                        val token = UserDatabase.currentUser?.token.toString()
                        loginResult.currentLoginStatus = LoginResult.loginStatusSuccess
                        loginResultLiveData.value = loginResult

                        Log.e("token", token)
                    }

                } else {
                    loginResult.currentLoginStatus = LoginResult.loginStatusFailed
                    loginResultLiveData.value = loginResult
                    Log.e("RETROFIT_ERROR", response.code().toString())
                }
            }
        }
    }

    fun loginBuyerRequest(username: String, password: String) {

        val jsonObject = JSONObject()

        jsonObject.put("username", username)
        jsonObject.put("password", password)

//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.loginBuyer(requestBody)
            val loginResult = LoginResult()

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Log.e("RETROFIT REQUEST", "${response.code()} + ${response.body()?.cust_id}")


                    if (response.body()?.cust_id == null || username.isEmpty() || username.isBlank()) {
                        loginResult.currentLoginStatus = LoginResult.loginStatusFailed
                        loginResultLiveData.value = loginResult
                        Log.e("RETROFIT_ERROR", response.code().toString())
                    } else if (response.body()?.cust_id != null) {


                        var responseCustomerId = -1

                        response.body()?.let { buyerData ->
                            responseCustomerId = buyerData.cust_id
                        }

                        if (responseCustomerId == -1) {
                            loginResult.currentLoginStatus = LoginResult.loginStatusFailed
                            loginResultLiveData.value = loginResult
                            Log.e("RETROFIT_ERROR", response.code().toString())
                            return@withContext
                        }


                        BuyerDatabase.connectIdWithCurrentUser(username, responseCustomerId)
                        BuyerDatabase.currentUser = BuyerDatabase.userList[username]
                        BuyerDatabase.currentUser?.username = username
                        BuyerDatabase.currentUser?.cust_id = responseCustomerId


                        Log.e("Member List", "${BuyerDatabase.userList}")
                        Log.e("Current Member:", "${BuyerDatabase.currentUser}")

                        val custId = BuyerDatabase.currentUser?.cust_id
                        loginResult.currentLoginStatus = LoginResult.loginStatusSuccess
                        loginResultLiveData.value = loginResult

                        Log.e("cust_id", custId.toString())
                    }

                } else {
                    loginResult.currentLoginStatus = LoginResult.loginStatusFailed
                    loginResultLiveData.value = loginResult
                    Log.e("RETROFIT_ERROR", response.code().toString())
                }
            }
        }
    }

    class LoginResult {

        var currentLoginStatus = loginStatusUnknown

        companion object {
            val loginStatusSuccess = "loginStatusSuccess"
            val loginStatusFailed = "loginStatusFailed"
            val loginStatusUnknown = "loginStatusUnknown"
        }
    }
}

