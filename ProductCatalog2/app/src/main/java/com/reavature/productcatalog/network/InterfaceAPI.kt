package com.reavature.productcatalog.network


import com.reavature.productcatalog.authmodel.*
import com.reavature.productcatalog.catalogmodel.CatalogResponse
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface InterfaceAPI {

//    @Header("Authorization") token:String

    @POST("catalog")
    suspend fun getCatalog(@Body requestBody: RequestBody, @Header("Authorization") token:String): Response<List<CatalogResponse>>

    @POST("register")
    suspend fun registerBusiness(@Body requestBody: RequestBody): Response<RegisterResponse>

    @POST("profileupdate")
    suspend fun updateBusiness(@Body requestBody: RequestBody, @Header("Authorization") token:String): Response<VendorProfileResponse>

    @POST("login")
    suspend fun loginUser(@Body requestBody: RequestBody):Response<LoginResponse>

    @POST("vendorprofile")
    suspend fun getVendorProfile(@Body requestBody: RequestBody, @Header("Authorization") token:String): Response<VendorProfileResponse>

    @POST("addproduct")
    suspend fun addProduct(@Body requestBody: RequestBody, @Header("Authorization")token: String) : Response<AddItemResponse>

    @POST("removeproduct")
    suspend fun removeProduct(@Body requestBody: RequestBody, @Header("Authorization")token: String) : Response<RemoveItemResponse>

    @POST("product")
    suspend fun getProduct(@Body requestBody: RequestBody, @Header("Authorization")token:String):Response<ProductResponse>

    @POST("updateproduct")
    suspend fun updateProduct(@Body requestBody: RequestBody, @Header("Authorization") token:String): Response<ProductResponse>

    @POST("orders")
    suspend fun getOrders(@Body requestBody: RequestBody, @Header("Authorization") token:String): Response<List<VendorOrderResponse>>

    // Buyer methods
    @POST("custreg")
    suspend fun registerBuyer(@Body requestBody: RequestBody): Response<RegisterBuyerResponse>

    @POST("custlogin")
    suspend fun loginBuyer(@Body requestBody: RequestBody):Response<LoginBuyerResponse>

    @POST("custprofile")
    suspend fun getBuyerProfile(@Body requestBody: RequestBody): Response<BuyerProfileResponse>

    @POST("updatecust")
    suspend fun updateBuyer(@Body requestBody: RequestBody): Response<BuyerProfileResponse>

    @POST("buy")
    suspend fun makePurchase(@Body requestBody: RequestBody): Response<PurchaseResponse>

    @GET("allproducts")
    suspend fun getAllProducts(): Response<List<AllProductsResponse>>

    @POST("custorders")
    suspend fun getBuyerOrders(@Body requestBody: RequestBody): Response<List<BuyerOrderResponse>>
}