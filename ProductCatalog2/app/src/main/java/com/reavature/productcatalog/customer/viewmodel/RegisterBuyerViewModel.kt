package com.reavature.productcatalog.customer.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.reavature.productcatalog.network.RetrofitService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class RegisterBuyerViewModel : ViewModel() {

    var registerBuyerAbility = false

    val registerByerResultLiveData: MutableLiveData<RegisterBuyerResult> by lazy {
        MutableLiveData<RegisterBuyerResult>()
    }

    fun registerBuyerRequset(
        username: String,
        password: String,
        email: String,
        name: String,
        phone: String,
        address: String,

        ) {

        val jsonObject = JSONObject()



        jsonObject.put("username", username)
        jsonObject.put("password", password)
        jsonObject.put("email", email)
        jsonObject.put("name", name)
        jsonObject.put("phone", phone)
        jsonObject.put("address", address)

//        Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//        Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.registerBuyer(requestBody)
            val registerResult = RegisterBuyerResult()
            val errorMessage = "Could not register user."

            withContext(Dispatchers.Main) {

                if (response.isSuccessful) {

                    Log.e("RETROFIT REQUEST", "${response.code()} + ${response.body()?.message}")

                    if (response.body()?.message == errorMessage || response.code() >= 400 || requestBody.equals("")) {
                        registerResult.currentRegisterStatus =
                            RegisterBuyerResult.registerStatusFailed
                        registerByerResultLiveData.value = registerResult
                        Log.e("RETROFIT_ERROR", response.code().toString())
                    } else if (response.body()?.message != null || response.body()?.message != errorMessage) {
                        registerResult.currentRegisterStatus = RegisterBuyerResult.registerStatusSuccess
                        registerByerResultLiveData.value = registerResult
                        Log.e("RETROFIT_SUCCESS", response.code().toString())
                    }
                } else {
                    registerResult.currentRegisterStatus = RegisterBuyerResult.registerStatusFailed
                    registerByerResultLiveData.value = registerResult
                    Log.e("RETROFIT_ERROR", response.code().toString())
                }
            }
        }
    }


    class RegisterBuyerResult {

        var currentRegisterStatus = registerStatusUnknown

        companion object {
            val registerStatusSuccess = "registerStatusSuccess"
            val registerStatusFailed = "registerStatusFailed"
            val registerStatusUnknown = "registerStatusUnknown"
        }
    }
}