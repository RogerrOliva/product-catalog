package com.reavature.productcatalog.vendor.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.ProductResponse
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.network.RetrofitService
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class ProductDetailViewModel: ViewModel() {

    var updateProductAbility = false

    val updateProductLiveData: MutableLiveData<UpdateProductResult> by lazy {
        MutableLiveData<UpdateProductResult>()
    }

    fun UpdateProductRequest(
        id:Int,
        item:String,
        description:String,
        img0:String,
        img1:String,
        img2:String,
        actual_price:Double,
        selling_price:Double,
        quantity:Double,
        unit:String,
        variant:String
    ) {

        val jsonObject = JSONObject()

        jsonObject.put("id", id)
        jsonObject.put("item", item)
        jsonObject.put("description", description)
        jsonObject.put("img0", img0)
        jsonObject.put("img1", img1)
        jsonObject.put("img2", img2)
        jsonObject.put("actual_price", actual_price)
        jsonObject.put("selling_price", selling_price)
        jsonObject.put("quantity", quantity)
        jsonObject.put("unit", unit)
        jsonObject.put("variants", variant)

//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = RetrofitService.instance.updateProduct(requestBody, "Bearer ${UserDatabase.currentUser?.token}")
            val updateProductResult = UpdateProductResult()
            val errorMessage =  "Could not update images."

            withContext(Dispatchers.Main) {


                if (response.isSuccessful) {

                    Log.e("RETROFIT REQUEST", "${response.code()} + ${response.body()?.message}")

                    if (response.body()?.message == errorMessage || response.code() >= 400 || requestBody.equals("")) {
                        updateProductResult.currentUpdateProductDataStatus = UpdateProductResult.updateProductStatusFailed
                        updateProductLiveData.value = updateProductResult
                        Log.e("RETROFIT_ERROR", response.code().toString())

                    } else if (response.body()?.message != null || response.body()?.message != errorMessage) {
                        updateProductResult.currentUpdateProductDataStatus = UpdateProductResult.updateProductStatusSuccess
                        updateProductLiveData.value = updateProductResult
                        Log.e("RETROFIT_SUCCESS", response.code().toString())
                    }
                } else {
                    updateProductResult.currentUpdateProductDataStatus = UpdateProductResult.updateProductStatusFailed
                    updateProductLiveData.value = updateProductResult
                    Log.e("RETROFIT_ERROR", response.code().toString())
                }
            }
        }
    }

    class UpdateProductResult{

        var currentUpdateProductDataStatus = updateProductStatusUnknown

//        var currentProductData: ProductResponse? = null


        companion object {
            val updateProductStatusSuccess = "productStatusSuccess"
            val updateProductStatusFailed = "productStatusFailed"
            val updateProductStatusUnknown = "productStatusUnknown"
        }
    }
}



