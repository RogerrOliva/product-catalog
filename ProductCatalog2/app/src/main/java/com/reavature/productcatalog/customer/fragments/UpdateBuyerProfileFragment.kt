package com.reavature.productcatalog.customer.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.reavature.productcatalog.MainActivity
import com.reavature.productcatalog.R
import com.reavature.productcatalog.customer.viewmodel.UpdateBuyerProfileViewModel
import com.reavature.productcatalog.databinding.FragmentUpdateBuyerProfileBinding
import androidx.lifecycle.Observer
import java.util.*

class UpdateBuyerProfileFragment : Fragment() {

    private var _binding: FragmentUpdateBuyerProfileBinding? = null

    private val binding: FragmentUpdateBuyerProfileBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        val myActivity: MainActivity = activity as MainActivity

        myActivity.checkItemInBottomNavigationBar(R.id.menu_item_update_profile)

        val updateBuyerProfileViewModel: UpdateBuyerProfileViewModel by viewModels()

        // !!! OBSERVER !!! //
        //======================================================================================================//
        val buyerDataObserver =
            Observer<UpdateBuyerProfileViewModel.BuyerLoadingStatus> { buyerStatus ->

                if (buyerStatus.currentBuyerStatus == UpdateBuyerProfileViewModel.BuyerLoadingStatus.buyerStatusSuccess) {

                    binding.buyerName.setText(buyerStatus.currentBuyerData?.name)
                    binding.buyerUsername.setText(buyerStatus.currentBuyerData?.username)
                    binding.buyerPassword.setText(buyerStatus.currentBuyerData?.password)
                    binding.buyerPasswordReType.setText(buyerStatus.currentBuyerData?.password)
                    binding.buyerEmail.setText(buyerStatus.currentBuyerData?.email)
                    binding.buyerPhoneNumber.setText(buyerStatus.currentBuyerData?.phone)
                    binding.buyerAddress.setText(buyerStatus.currentBuyerData?.address)
                }
            }

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        updateBuyerProfileViewModel.buyerStatusLiveData.observe(
            viewLifecycleOwner,
            buyerDataObserver
        )
        //======================================================================================================//

        updateBuyerProfileViewModel.profileRequest()



        _binding = FragmentUpdateBuyerProfileBinding.inflate(inflater, container, false)


        binding.btnUpdateProfile.setOnClickListener {

            when {
                binding.buyerName.editableText.toString() == "" -> binding.buyerName.error =
                    "Name can't be empty"
                binding.buyerUsername.editableText.toString() == "" -> binding.buyerUsername.error =
                    "Username can't be empty"
                binding.buyerPassword.editableText.toString() == "" -> binding.buyerPassword.error =
                    "Password can't be empty"
                binding.buyerPasswordReType.editableText.toString() == "" -> binding.buyerPasswordReType.error =
                    "Password can't be empty"
                binding.buyerEmail.editableText.toString() == "" -> binding.buyerEmail.error =
                    "Email can't be empty"
                binding.buyerPhoneNumber.editableText.toString() == "" -> binding.buyerPhoneNumber.error =
                    "Phone can't be empty"
                binding.buyerAddress.editableText.toString() == "" -> binding.buyerAddress.error =
                    "Address can't be empty"
                binding.buyerPassword.text.toString() != binding.buyerPasswordReType.text.toString() -> Toast.makeText(
                    context,
                    "Passwords is not the same",
                    Toast.LENGTH_SHORT
                ).show()

                else -> updateBuyerProfileViewModel.updateAbility = true
            }

            if (updateBuyerProfileViewModel.updateAbility) {
                updateBuyerProfileViewModel.updateUserRequest(

                    binding.buyerUsername.editableText.toString(),
                    binding.buyerPassword.editableText.toString(),
                    binding.buyerEmail.editableText.toString(),
                    binding.buyerName.editableText.toString(),
                    binding.buyerPhoneNumber.editableText.toString(),
                    binding.buyerAddress.editableText.toString()
                )
                findNavController().navigate(R.id.action_global_buyerMainMenuFragment)
                Toast.makeText(context, "User was updated", Toast.LENGTH_SHORT).show()
            }

        }

        binding.tapForMainMenuButton.setOnClickListener {
            findNavController().navigate(R.id.action_global_buyerMainMenuFragment)
        }

        binding.tapForMainMenuTextView.setOnClickListener {
            findNavController().navigate(R.id.action_global_buyerMainMenuFragment)
        }
        return binding.root
    }

}