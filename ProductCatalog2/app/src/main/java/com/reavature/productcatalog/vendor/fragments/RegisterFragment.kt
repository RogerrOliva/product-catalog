package com.reavature.productcatalog.vendor.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.reavature.productcatalog.databinding.FragmentRegisterBinding
import com.reavature.productcatalog.vendor.viewmodel.RegisterViewModel
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.reavature.productcatalog.R


class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null

    private val binding: FragmentRegisterBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        /**
         * Initializing View Model
         */
        val registerViewModel: RegisterViewModel by viewModels()

        /**
         * Observer
         */

        val registerResultObserver = Observer<RegisterViewModel.RegisterResult> { registerResult ->
            if (registerResult.currentRegisterStatus == RegisterViewModel.RegisterResult.registerStatusFailed) {
                Log.e("REGISTER_REQUEST", "Failed Response")
                showFailedRegisterDialog()
            } else if (registerResult.currentRegisterStatus == RegisterViewModel.RegisterResult.registerStatusSuccess) {
                Log.e("REGISTER_REQUEST", "Success Response")
                showSucessRegisterDialog()

            }
        }

        /**
         *  Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
         */
        registerViewModel.registerResultLiveData.observe(viewLifecycleOwner, registerResultObserver)

        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)

        val spinner = binding.businessCategorySpinner
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.business_category_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        /**
         * On click on btnRegister, API will be called.
         *
         * I will comment this out for now because
         * I replaced the BASE URL thus my recycler view will no longer have data from APIARY.
         * I want to show recycler view progress for 5/5/2021
         */
        binding.btnRegister.setOnClickListener {

            when {
                binding.ownerName.editableText.toString() == "" -> binding.ownerName.error =
                    "Owner name can't be empty"
                binding.businessName.editableText.toString() == "" -> binding.businessName.error =
                    "Business name can't be empty"
                binding.businessUsername.editableText.toString() == "" -> binding.businessUsername.error =
                    "Username can't be empty"
                binding.businessPassword.editableText.toString() == "" -> binding.businessPassword.error =
                    "Password can't be empty"
                binding.businessPasswordReType.editableText.toString() == "" -> binding.businessPasswordReType.error =
                    "Password can't be empty"
                binding.businessPhoneNumber.editableText.toString() == "" -> binding.businessPhoneNumber.error =
                    "Phone can't be empty"
                binding.businessAddress.editableText.toString() == "" -> binding.businessAddress.error =
                    "Address can't be empty"
                binding.businessCity.editableText.toString() == "" -> binding.businessCity.error =
                    "City can't be empty"
                binding.businessState.editableText.toString() == "" -> binding.businessState.error =
                    "State can't be empty"
                binding.businessZipcode.editableText.toString() == "" -> binding.businessZipcode.error =
                    "Zipcode can't be empty"
                binding.businessPassword.text.toString() != binding.businessPasswordReType.text.toString() -> Toast.makeText(
                    context,
                    "Passwords is not the same",
                    Toast.LENGTH_SHORT
                ).show()

                else -> registerViewModel.registerAbility = true
            }

            if (registerViewModel.registerAbility) {
                registerViewModel.registerRequest(
                    binding.ownerName.editableText.toString(),
                    binding.businessName.editableText.toString(),
                    binding.businessUsername.editableText.toString(),
                    binding.businessPassword.editableText.toString(),
                    binding.businessPhoneNumber.editableText.toString(),
                    binding.businessAddress.editableText.toString(),
                    binding.businessCity.editableText.toString(),
                    binding.businessState.editableText.toString(),
                    binding.businessZipcode.editableText.toString(),
                    binding.businessCategorySpinner.selectedItem.toString()
                )
            }
        }

        binding.tapForLoginButton.setOnClickListener {
            findNavController().navigate(R.id.action_global_loginFragment)
        }

        binding.tapForLoginTextView.setOnClickListener {
            findNavController().navigate(R.id.action_global_loginFragment)
        }
        return binding.root
    }


    private fun showSucessRegisterDialog() {

        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Registration is successful")
            .setMessage("It's time to add products!")

            .setPositiveButton("Thank you") { _, _ ->
                findNavController().navigate(R.id.action_global_loginFragment)
            }
            .show()

    }

    private fun showFailedRegisterDialog() {

        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Registration Failed")
            .setMessage("Registration Failed, Please Try Again")

            .setPositiveButton("Try Again") { dialog, Which ->

            }
            .show()

    }
}
