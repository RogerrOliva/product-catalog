package com.reavature.productcatalog.customer.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.reavature.productcatalog.authmodel.BuyerDatabase
import com.reavature.productcatalog.authmodel.BuyerOrderResponse
import com.reavature.productcatalog.customer.adapters.OrderListBuyerAdapter
import com.reavature.productcatalog.databinding.OrderListBuyerFragmentBinding
import com.reavature.productcatalog.network.RetrofitService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class OrderListBuyerFragment : Fragment() {

    private var _binding: OrderListBuyerFragmentBinding? = null

    private val binding: OrderListBuyerFragmentBinding
        get() = _binding!!


    private lateinit var orderRecyclerView: RecyclerView

    private lateinit var recyclerViewAdapter: OrderListBuyerAdapter

    private lateinit var orderData: List<BuyerOrderResponse>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = OrderListBuyerFragmentBinding.inflate(inflater, container, false)
        orderRecyclerView = binding.recyclerviewBuyerOrders
        getOrders()
        return binding.root
    }

    private fun getOrders() {

        val jsonObject = JSONObject()
        jsonObject.put("id", BuyerDatabase.currentUser?.cust_id)
//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response

//            ,"Bearer ${UserDatabase.currentUser?.token.toString()}
            val response = RetrofitService.instance.getBuyerOrders(
                requestBody
            )

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    response.body()?.let { BuyerOrderResponse ->
                        orderData = BuyerOrderResponse
                        Log.e("ResponseBody", "${response.body()}")
                        recyclerViewAdapter = OrderListBuyerAdapter(orderData, requireContext())
                        orderRecyclerView.adapter = recyclerViewAdapter
                    }
                }
                if (response.body() == null) {
                    orderRecyclerView.visibility = View.GONE
                    binding.emptyView.visibility = View.VISIBLE
                    binding.icEmpty.visibility = View.VISIBLE
                }
            }
        }
    }
}