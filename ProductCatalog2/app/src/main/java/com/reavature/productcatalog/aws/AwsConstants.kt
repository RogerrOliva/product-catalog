package com.reavature.productcatalog.aws

import com.amazonaws.regions.Regions

object AwsConstants {

    val COGNITO_IDENTITY_ID: String = "us-east-2:36fc69a5-2f40-4155-ad2f-03ca07dc1a26"
    val COGNITO_REGION: Regions = Regions.US_EAST_2
    val BUCKET_NAME:String = "lonlonbucket"

    val S3_URL: String = "https://$BUCKET_NAME.s3.us-east-2.amazonaws.com/"
    val folderPath = ""


}