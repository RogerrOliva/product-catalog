package com.reavature.productcatalog.customer.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.contentValuesOf
import androidx.lifecycle.LiveData
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.AllProductsResponse
import com.reavature.productcatalog.authmodel.BuyerDatabase
import com.reavature.productcatalog.authmodel.UserDatabase
import com.reavature.productcatalog.catalogmodel.CatalogResponse
import com.reavature.productcatalog.network.RetrofitService
import com.reavature.productcatalog.vendor.fragments.VendorCatalogFragment
import com.reavature.productcatalog.vendor.fragments.viewPagerContainerDirections
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.customer_catalog_linear_list_item.view.*
import kotlinx.android.synthetic.main.customer_catalog_list_item.view.*
import kotlinx.android.synthetic.main.customer_catalog_list_item.view.btn_purchase
import kotlinx.android.synthetic.main.fragment_add_item.view.*
import kotlinx.android.synthetic.main.vendor_catalog_list_item.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Response
import java.security.AccessController.getContext
import java.util.*
import kotlin.collections.ArrayList

class CustomerCatalogLinearAdapter(
    private val catalogList: List<AllProductsResponse>,
    val context: Context
) :
    RecyclerView.Adapter<CustomerCatalogLinearAdapter.CatalogViewHolder>(), Filterable {

    private var catalogListFiltered = catalogList

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CustomerCatalogLinearAdapter.CatalogViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.customer_catalog_linear_list_item, parent, false)
        return CatalogViewHolder(itemView)
    }

    override fun onBindViewHolder(
        holder: CustomerCatalogLinearAdapter.CatalogViewHolder,
        position: Int
    ) {

        val catalogData = catalogListFiltered[position]
        val prodID = catalogData.product_id
        val venID = catalogData.vendor_id
        val custID = BuyerDatabase.currentUser?.cust_id
        val productName = catalogData.item
        val prodDesc = catalogData.description
        val prodVar = catalogData.variants
        val prodUnit = catalogData.unit
        val price = catalogData.actual_price
        val disPrice = catalogData.selling_price

        Log.e("ProductID", "$prodID, $venID, $productName")


        if (catalogData.img0 != "") {
            Picasso.get().load(catalogData.img0).into(holder.productImg0)
        }

        holder.productName.text = productName
        holder.productPrice.text = "$ $price"

        holder.itemView.btn_purchase.setOnClickListener {
            Log.e("buy Icon", "try buy")
            if (custID != null) {
                showPurchaseAlertDialog(productName, prodID, custID, venID, disPrice)
            }
        }

        holder.itemView.btn_product_details_linear.setOnClickListener {
            if (custID != null) {
                showProductDetails(
                    productName,
                    prodDesc,
                    prodVar,
                    prodUnit,
                    prodID,
                    custID,
                    venID,
                    disPrice
                )
            }

        }

//        holder.productPrice.text = catalogData.price
//        holder.productUnit.text = catalogData.unit
//        holder.productDesc.text = catalogData.description

    }



override fun getItemCount(): Int {
    return catalogListFiltered.size
}

override fun getFilter(): Filter {
    return object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val searchString = constraint.toString()
            catalogListFiltered = if (searchString.isEmpty()) {
                catalogList
            } else {
                val filterList = ArrayList<AllProductsResponse>()
                catalogListFiltered.forEach { row ->
                    if (row.item.toLowerCase(Locale.ROOT).trim()
                            .contains(searchString.toLowerCase(Locale.ROOT))
                    ) {//if // the last name contains the string
                        filterList.add(row)
                    }

                }

                filterList
            }

            val filterResults = FilterResults()
            filterResults.values = catalogListFiltered
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            if (results?.values is ArrayList<*>) {
                catalogListFiltered = results.values as ArrayList<AllProductsResponse>
                notifyDataSetChanged()
            }
        }
    }
}


inner class CatalogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var productImg0: ImageView = itemView.findViewById(R.id.img_product0)

    //        var productImg1: ImageView = itemView.findViewById(R.id.img_product1)
//        var productImg3: ImageView = itemView.findViewById(R.id.img_product3)
    var productName: TextView = itemView.findViewById(R.id.tv_product_name)
    var productPrice: TextView = itemView.findViewById(R.id.tv_product_price)
//        var productUnit: TextView = itemView.findViewById(R.id.tv_product_unit)
//        var productDesc: TextView = itemView.findViewById(R.id.tv_product_description)


}


    private fun buyProductRequest(
        custID: Int,
        venID: Int,
        prodID: Int,
        prodName: String,
        disPrice: Double
    ) {

        val jsonObject = JSONObject()
        jsonObject.put("cust_id", custID)
        jsonObject.put("vendor_id", venID)
        jsonObject.put("product_id", prodID)
        jsonObject.put("Product_name", prodName)
        jsonObject.put("total", disPrice)
//             Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

//            // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response


            val response = RetrofitService.instance.makePurchase(requestBody)

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Log.e("PURCHASE_REQUEST_SUCCESS", "PURCHASED")
                    showSucessPurchaseDialog()
                } else {
                    Log.e("PURCHASED_REQUEST_FAILED", "FAILED")
                    showFailedPurchasedDialog()
                }
                if (response.body() == null) {
                    Log.e("PURCHASED_REQUEST_FAILED", "FAILED")
                    showFailedPurchasedDialog()

                }
            }
        }
    }

    private fun showPurchaseAlertDialog(
        productName: String,
        productID: Int,
        custID: Int,
        vendID: Int,
        price: Double
    ) {

        MaterialAlertDialogBuilder(context)
            .setTitle("Purchase")
            .setMessage("You are purchasing a product. Are you sure you want to purchase?")
            .setNeutralButton("Cancel") { _, _ ->
                Log.e("Cancel", "cancel")
            }
            .setNegativeButton("No") { _, _ ->
                Log.e("No", "no")
            }
            .setPositiveButton("Yes") { _, _ ->
                Log.e("Yes", "Call Buy API")
                buyProductRequest(custID, vendID, productID, productName, price)

            }
            .show()

    }


    private fun showFailedPurchasedDialog() {

        MaterialAlertDialogBuilder(context)
            .setTitle("Purchased Failed")
            .setMessage("Sorry something went wrong, please try again")

            .setPositiveButton("Try Again") { dialog, Which ->
                Log.e("okay", "Okay, try again")
            }

            .show()

    }

    private fun showSucessPurchaseDialog() {

        MaterialAlertDialogBuilder(context)
            .setTitle("Purchase Successful")
            .setMessage("Product has been successfully Purchased, Your vendor will fulfill your order.")
            .setNegativeButton("View Your Orders") { _, _ ->

            }
            .setPositiveButton("Thank you") { _, _ ->
                Log.e("Yes", "Purchase successful")
            }
            .show()

    }

    private fun showProductDetails(
        productName: String,
        productDesc: String,
        productVariant: String,
        productUnit: String,
        productID: Int,
        custID: Int,
        vendID: Int,
        price: Double
    ) {

        MaterialAlertDialogBuilder(context)
            .setTitle(productName)
            .setMessage(" Variants: $productVariant \n Unit: $productUnit \n Product Details: $productDesc. \n ")

            .setPositiveButton("Buy") { _, _ ->
                Log.e("Yes", "Call Delete API")
                buyProductRequest(custID, vendID, productID, productName, price)
            }
            .show()

    }



}