package com.reavature.productcatalog

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.reavature.productcatalog.databinding.FragmentRegisterBinding
import com.reavature.productcatalog.vendor.viewmodel.RegisterViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import androidx.lifecycle.Observer
import com.reavature.productcatalog.databinding.FragmentRegistrationChoiceBinding
import kotlinx.android.synthetic.main.fragment_register.*


class ChoiceRegistrationFragment : Fragment() {

    private var _binding: FragmentRegistrationChoiceBinding? = null

    private val binding: FragmentRegistrationChoiceBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        /**
         * Initializing View Model
         */

        /**
         * Observer
         */

        /**
         *  Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
         */

        // Inflate the layout for this fragment
        _binding = FragmentRegistrationChoiceBinding.inflate(inflater, container, false)




        binding.btnRegisterAsBuyer.setOnClickListener {
            findNavController().navigate(R.id.action_global_registerBuyerFragment)
        }

        binding.btnRegisterAsOwner.setOnClickListener {
            findNavController().navigate(R.id.action_global_registerFragment)
        }

        binding.tapForLoginButton.setOnClickListener {
            findNavController().navigate(R.id.action_global_loginFragment)
        }

        binding.tapForLoginTextView.setOnClickListener{
            findNavController().navigate(R.id.action_global_loginFragment)
        }
        return binding.root
    }
}
