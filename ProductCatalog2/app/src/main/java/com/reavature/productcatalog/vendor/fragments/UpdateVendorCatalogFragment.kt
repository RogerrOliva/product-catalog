package com.reavature.productcatalog.vendor.fragments

import  android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.reavature.productcatalog.R
import com.reavature.productcatalog.databinding.FragmentUpdateProfileBinding
import com.reavature.productcatalog.databinding.FragmentUpdateVendorCatalogBinding
import com.reavature.productcatalog.databinding.FragmentVendorcatalogBinding


class UpdateVendorCatalogFragment : Fragment() {


    private var _binding: FragmentUpdateVendorCatalogBinding? = null

    private val binding: FragmentUpdateVendorCatalogBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentUpdateVendorCatalogBinding.inflate(inflater,container,false)

        
        // Inflate the layout for this fragment
        return binding.root
    }

}