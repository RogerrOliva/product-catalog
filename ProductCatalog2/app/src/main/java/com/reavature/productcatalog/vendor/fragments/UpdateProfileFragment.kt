package com.reavature.productcatalog.vendor.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.reavature.productcatalog.MainActivity
import com.reavature.productcatalog.R
import com.reavature.productcatalog.databinding.FragmentUpdateProfileBinding
import com.reavature.productcatalog.vendor.viewmodel.UpdateProfileViewModel

class UpdateProfileFragment : Fragment() {

    private var _binding: FragmentUpdateProfileBinding? = null

    private val binding: FragmentUpdateProfileBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        val myActivity: MainActivity = activity as MainActivity

        myActivity.showNavigationBottom()

        myActivity.checkItemInBottomNavigationBar(R.id.menu_item_update_profile)

        val updateProfileViewModel: UpdateProfileViewModel by viewModels()

        // !!! OBSERVER !!! //
        //======================================================================================================//
        val userDataObserver = Observer<UpdateProfileViewModel.UserLoadingStatus> { userStatus ->

            if (userStatus.currentUserStatus == UpdateProfileViewModel.UserLoadingStatus.userStatusSuccess) {
                binding.ownerName.setText(userStatus.currentUserData?.owner)
                binding.businessName.setText(userStatus.currentUserData?.name)
                binding.businessUsername.setText(userStatus.currentUserData?.username)
                binding.businessPassword.setText(userStatus.currentUserData?.password)
                binding.businessPasswordReType.setText(userStatus.currentUserData?.password)
                binding.businessPhoneNumber.setText(userStatus.currentUserData?.phone)
                binding.businessAddress.setText(userStatus.currentUserData?.street)
                binding.businessCity.setText(userStatus.currentUserData?.city)
                binding.businessState.setText(userStatus.currentUserData?.state)
                binding.businessZipcode.setText(userStatus.currentUserData?.zipcode)
                userStatus.currentUserData?.category?.let {
                    val indexOf =
                        getResources().getStringArray(R.array.business_category_array).indexOf(it)
                    binding.businessCategorySpinner.setSelection(indexOf)
                }
            }
        }

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        updateProfileViewModel.userStatusLiveData.observe(viewLifecycleOwner, userDataObserver)
        //======================================================================================================//

        updateProfileViewModel.profileRequest()


        _binding = FragmentUpdateProfileBinding.inflate(inflater, container, false)

        val spinner = binding.businessCategorySpinner
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.business_category_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        binding.btnUpdateProfile.setOnClickListener {

            when {
                binding.ownerName.editableText.toString() == "" -> binding.ownerName.error =
                    "Owner name can't be empty"
                binding.businessName.editableText.toString() == "" -> binding.businessName.error =
                    "Business name can't be empty"
                binding.businessUsername.editableText.toString() == "" -> binding.businessUsername.error =
                    "Username can't be empty"
                binding.businessPassword.editableText.toString() == "" -> binding.businessPassword.error =
                    "Password can't be empty"
                binding.businessPasswordReType.editableText.toString() == "" -> binding.businessPasswordReType.error =
                    "Password can't be empty"
                binding.businessPhoneNumber.editableText.toString() == "" -> binding.businessPhoneNumber.error =
                    "Phone can't be empty"
                binding.businessAddress.editableText.toString() == "" -> binding.businessAddress.error =
                    "Address can't be empty"
                binding.businessCity.editableText.toString() == "" -> binding.businessCity.error =
                    "City can't be empty"
                binding.businessState.editableText.toString() == "" -> binding.businessState.error =
                    "State can't be empty"
                binding.businessZipcode.editableText.toString() == "" -> binding.businessZipcode.error =
                    "Zipcode can't be empty"
                binding.businessPassword.text.toString() != binding.businessPasswordReType.text.toString() -> Toast.makeText(
                    context,
                    "Passwords is not the same",
                    Toast.LENGTH_SHORT
                ).show()

                else -> updateProfileViewModel.updateAbility = true
            }

            if (updateProfileViewModel.updateAbility) {
                updateProfileViewModel.updateUserRequest(
                    binding.ownerName.editableText.toString(),
                    binding.businessName.editableText.toString(),
                    binding.businessUsername.editableText.toString(),
                    binding.businessPassword.editableText.toString(),
                    binding.businessPhoneNumber.editableText.toString(),
                    binding.businessAddress.editableText.toString(),
                    binding.businessCity.editableText.toString(),
                    binding.businessState.editableText.toString(),
                    binding.businessZipcode.editableText.toString(),
                    binding.businessCategorySpinner.selectedItem.toString()
                )
                findNavController().navigate(R.id.action_global_mainMenuFragment)
                Toast.makeText(context, "User was updated", Toast.LENGTH_SHORT).show()
            }
        }

        binding.tapForMainMenuButton.setOnClickListener {
            findNavController().navigate(R.id.action_global_mainMenuFragment)
        }

        binding.tapForMainMenuTextView.setOnClickListener {
            findNavController().navigate(R.id.action_global_mainMenuFragment)
        }
        return binding.root
    }

}
