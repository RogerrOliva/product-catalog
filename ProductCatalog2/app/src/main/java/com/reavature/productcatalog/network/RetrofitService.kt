package com.reavature.productcatalog.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
        /**
         *  Mock API
         * */
//    private const val BASE_URL = "https://private-e1fed-urahara.apiary-mock.com/"

    /**
     *  Real API
     */

    private const val BASE_URL = "https://lon-lon-api.herokuapp.com/"

    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor { chain ->
            val original = chain.request()

            val log = HttpLoggingInterceptor()

            log.level = HttpLoggingInterceptor.Level.BODY

            val requestBuilder = original.newBuilder().method(original.method, original.body)

            val request = requestBuilder.build()

            chain.proceed(request)

        }.build()

    val instance: InterfaceAPI by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        retrofit.create(InterfaceAPI::class.java)
    }


}