package com.reavature.productcatalog.authmodel

//"username": “vladbuy”,
//"password": "123",
//"email": "bob@yahoo.com",
//"name": "Bob Black",
//"phone": "9319439432",
//"address": "123 Black Street"

data class RegisterBuyerResponse (
    val username: String,
    val password: String,
    val email: String,
    val name: String,
    val phone: String,
    val address: String,
    val message: String
    )