package com.reavature.productcatalog.authmodel

data class LoginBuyerResponse(
    var type: String,
    var cust_id: Int
)

data class BuyerUser(
    var username: String,
    var cust_id: Int

)

object BuyerDatabase {

    var currentUser: BuyerUser? = null
    var userList: MutableMap<String,BuyerUser> = mutableMapOf()


    fun connectIdWithCurrentUser(username:String, cust_id:Int):Boolean {
        userList[username] = BuyerUser(username,cust_id)
        return true
    }
}
