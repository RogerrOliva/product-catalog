package com.reavature.productcatalog.vendor.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.reavature.productcatalog.MainActivity
import com.reavature.productcatalog.R
import kotlinx.android.synthetic.main.fragment_view_pager_container.*

class viewPagerContainer : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val myActivity: MainActivity = activity as MainActivity

        myActivity.checkItemInBottomNavigationBar(R.id.menu_item_view_catalog_page)

        return inflater.inflate(R.layout.fragment_view_pager_container, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

            val adapter = ViewPagerAdapter(childFragmentManager)
            adapter.addFragment(VendorCatalogFragment(),"")
            adapter.addFragment(VendorCatalogFragmentLinear(),"")
            viewPager.adapter = adapter
            tabs.setupWithViewPager(viewPager)
            tabs.getTabAt(0)!!.setIcon(R.drawable.ic_baseline_grid_view_24)
            tabs.getTabAt(1)!!.setIcon(R.drawable.ic_baseline_dehaze_24)


    }

}