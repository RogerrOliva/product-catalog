package com.reavature.productcatalog.vendor.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.ContentUris
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ImageView
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.reavature.productcatalog.MainActivity
import com.reavature.productcatalog.R
import com.reavature.productcatalog.authmodel.ImageDatabase
import com.reavature.productcatalog.aws.AWSUtils
import com.reavature.productcatalog.aws.AwsConstants
import com.reavature.productcatalog.databinding.FragmentAddItemBinding
import com.reavature.productcatalog.vendor.viewmodel.AddItemViewModel
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import java.io.File
import java.io.IOException
import java.net.URISyntaxException
import java.text.SimpleDateFormat
import java.util.*


class AddItemFragment : Fragment() , AWSUtils.OnAwsImageUploadListener, EasyPermissions.PermissionCallbacks,
    EasyPermissions.RationaleCallbacks {

    private var _binding: FragmentAddItemBinding? = null

    private val binding: FragmentAddItemBinding
        get() = _binding!!

    private val select_picture:Int = 200
    private val Request_Image_Capture = 1

    private lateinit var image: ImageView
    private lateinit var image2: ImageView
    private lateinit var image3: ImageView
    private var imageClicked:Int = 0

    var currentPhotoPath: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val myActivity: MainActivity = activity as MainActivity

        myActivity.showNavigationBottom()

        myActivity.checkItemInBottomNavigationBar(R.id.menu_item_add_items_page)

        val addItemViewModel: AddItemViewModel by viewModels()


        /**     Observer        */

        val addItemResultObserver = Observer<AddItemViewModel.AddItemResult> { addItemResult ->

            if (addItemResult.currentAddItemStatus == AddItemViewModel.AddItemResult.addItemStatusFailed) {
                failedShowAlertDialog()
                Log.e("ADDITEM_REQUEST_FAILED", "FAILED")
            } else if (addItemResult.currentAddItemStatus == AddItemViewModel.AddItemResult.addItemStatusSuccess) {
                successShowAlertDialog()
                Log.e("ADDITEM_REQUEST_SUCCESS", "SUCCESS")
            }
        }

        addItemViewModel.addItemResultLiveData.observe(viewLifecycleOwner, addItemResultObserver)

        _binding = FragmentAddItemBinding.inflate(inflater, container, false)

        // Inflate the layout for this fragment


        val btnSubmit: Button = binding.btnAdd

        image = binding.ivMain
        image2 = binding.ivSecondary
        image3 = binding.ivThird

        val spinner = binding.unitSpin

        Log.d("Picture", "$image")

        ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.unit_type_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }

        image.setOnClickListener {

            if (hasExternalStorageWritePermission()) {
                selectImage()
            } else {
                EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(
                        this,
                        1000,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                        .setRationale("requires storage permission")
                        .setPositiveButtonText("Granted")
                        .setNegativeButtonText("Cancel")
                        .build()
                )
            }
            imageClicked = 1
        }

        image2.setOnClickListener {
            if (hasExternalStorageWritePermission()) {
                selectImage()
            } else {
                EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(
                        this,
                        1000,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                        .setRationale("requires storage permission")
                        .setPositiveButtonText("Granted")
                        .setNegativeButtonText("Cancel")
                        .build()
                )
            }
            imageClicked = 2
        }

        image3.setOnClickListener {
            if (hasExternalStorageWritePermission()) {
                selectImage()
            } else {
                EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(
                        this,
                        1000,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                        .setRationale("requires storage permission")
                        .setPositiveButtonText("Granted")
                        .setNegativeButtonText("Cancel")
                        .build()
                )
            }
            imageClicked = 3

        }

        btnSubmit.setOnClickListener {
//
//            Log.e("Image0",ImageDatabase.imageList.component1())
//            Log.e("Image1",ImageDatabase.imageList.component2())
//            Log.e("Image2",ImageDatabase.imageList.component3())

            try {

                Log.e("Image0", ImageDatabase.imageList[0])
                Log.e("Image1", ImageDatabase.imageList[1])
                Log.e("Image2", ImageDatabase.imageList[2])
            } catch (e: IndexOutOfBoundsException) {
                e.stackTrace
            }


            when {
                binding.etName.text.toString() == "" -> binding.etName.error =
                    "Product name cannot be empty"
                binding.etPrice.text.toString()
                    .toDoubleOrNull() == null || binding.etPrice.text.toString()
                    .toDouble() <= 0.0 -> binding.etPrice.error = "Price is an valid"
                binding.etDPrice.text.toString()
                    .toDoubleOrNull() == null || binding.etDPrice.text.toString()
                    .toDouble() <= 0.0 -> binding.etDPrice.error = "Discount price is invalid"
                binding.etDesc.text.toString() == "" -> binding.etDesc.error =
                    "Product Description is empty"
                binding.etQuantity.text.toString()
                    .toDoubleOrNull() == null || binding.etQuantity.text.toString()
                    .toDouble() <= 0.0 -> binding.etQuantity.error = "Product Quantity is invalid"

                else -> addItemViewModel.addItemAbility = true
            }

            /**
             * {
            "item":"Sample",
            "description":"sample description",
            "variants":"blue",
            "img0":"https://www.deanfoods.com/wp-content/uploads/2017/12/purity-dp_gal_2rf-357x640.png",
            "img1":"https://schachtgroves.com/wp-content/uploads/2020/10/1_0002_valencia.jpg",
            "img2":"https://i0.wp.com/stdades.com/wp-content/uploads/2018/04/shop-1-img-8.png?resize=600%2C600&ssl=1",
            "actual_price":10.0,
            "selling_price":5.0,
            "quantity":1.0,
            "unit":"3kg"
            }
             *
             */

            try {

                if (addItemViewModel.addItemAbility) {
                    addItemViewModel.addItemRequest(
                        binding.etName.text.toString(),
                        binding.etDesc.text.toString(),
                        binding.etVariants.text.toString(),
                        ImageDatabase.imageList[0],
                        ImageDatabase.imageList[1],
                        ImageDatabase.imageList[2],
                        binding.etPrice.text.toString().toDouble(),
                        binding.etDPrice.text.toString().toDouble(),
                        binding.etQuantity.text.toString().toDouble(),
                        binding.etUnits.text.toString()
                    )

                    ImageDatabase.imageList.clear()

                    binding.etName.text?.clear()
                    binding.etDesc.text?.clear()
                    binding.etPrice.text?.clear()
                    binding.etDPrice.text?.clear()
                    binding.etQuantity.text?.clear()
                    binding.etUnits.text?.clear()
                    binding.etVariants.text?.clear()

                    image.setImageResource(android.R.drawable.ic_menu_camera)
                    image2.setImageResource(android.R.drawable.ic_menu_camera)
                    image3.setImageResource(android.R.drawable.ic_menu_camera)
                }

                addItemViewModel.addItemAbility = false

            } catch (e: IndexOutOfBoundsException) {
                addItemViewModel.addItemRequest(
                    binding.etName.text.toString(),
                    binding.etDesc.text.toString(),
                    binding.etVariants.text.toString(),
                    "",
                    "",
                    "",
                    binding.etPrice.text.toString().toDouble(),
                    binding.etDPrice.text.toString().toDouble(),
                    binding.etQuantity.text.toString().toDouble(),
                    binding.etUnits.text.toString()
                )

                ImageDatabase.imageList.clear()

                binding.etName.text?.clear()
                binding.etDesc.text?.clear()
                binding.etPrice.text?.clear()
                binding.etDPrice.text?.clear()
                binding.etQuantity.text?.clear()
                binding.etUnits.text?.clear()
                binding.etVariants.text?.clear()

                image.setImageResource(android.R.drawable.ic_menu_camera)
                image2.setImageResource(android.R.drawable.ic_menu_camera)
                image3.setImageResource(android.R.drawable.ic_menu_camera)
            }
            addItemViewModel.addItemAbility = false
        }
        return binding.root
    }

    private fun selectImage() {

        val items = arrayOf<CharSequence>("Take Photo", "Gallery", "Cancel")

        val builder = AlertDialog.Builder(this.requireContext())

        builder.setTitle("Add Photo")

        builder.setItems(items){ dialog, item->
            when
            {
                items[item] == "Take Photo" -> dispatchTakePictureIntent()
                items[item] == "Gallery" -> galleryIntent()
                items[item] == "Cancel" -> dialog.dismiss()
            }
        }
        builder.show()
    }

    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), select_picture)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK && requestCode == select_picture) {
            val selectedImageUri = data?.data
            val path: String? = getPath(selectedImageUri!!)

            AWSUtils(this.requireContext(), path!!,this, AwsConstants.folderPath).beginUpload()

            if (imageClicked == 1)
                image.setImageURI(selectedImageUri)
            if (imageClicked == 2)
                image2.setImageURI(selectedImageUri)
            if (imageClicked == 3)
                image3.setImageURI(selectedImageUri)
        }

        if(requestCode == Request_Image_Capture && resultCode == RESULT_OK){

            val imageBitmap = File(currentPhotoPath)
            val imageToUri = imageBitmap.toPath()

            if (imageToUri != null) {
                AWSUtils(this.requireContext(), imageToUri.toString(), this, AwsConstants.folderPath).beginUpload()
            }

            if (imageClicked == 1)
                if(imageBitmap.exists())
                    image.setImageURI(Uri.fromFile(imageBitmap))
            if (imageClicked == 2)
                if(imageBitmap.exists())
                    image2.setImageURI(Uri.fromFile(imageBitmap))
            if (imageClicked == 3)
                if (imageBitmap.exists())
                    image3.setImageURI(Uri.fromFile(imageBitmap))
        }
    }

    private fun dispatchTakePictureIntent(){

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true)

        try{

            var pictureFile: File = createImageFile()

            val photoURI: Uri = FileProvider.getUriForFile(
                this.requireContext(),
                "com.reavature.productcatalog.fileprovider",
                pictureFile
            )
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            startActivityForResult(cameraIntent, Request_Image_Capture)
            Log.d("Test", currentPhotoPath)
        } catch (e:Exception){
            Log.d("Test", "I don't know any more someone help me please because i don't know what i am doing anymore")
        }

    }

    @Throws(IOException::class)
    private fun createImageFile(): File{

        val timeStamp:String = SimpleDateFormat("MMddyyyyHHmmss").format(Date())
        val pictureFile = "Product_$timeStamp"
        val storageDir: File? = this.requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val imagePath = File.createTempFile(pictureFile, ".jpg", storageDir)

        currentPhotoPath = imagePath.absolutePath
        return imagePath
    }


    @SuppressLint("NewApi", "Recycle")
    @Throws(URISyntaxException::class)
    private fun getPath(selectedImageUri: Uri): String? {
        var uri = selectedImageUri
        var selection:String? = null
        var selectionArgs: Array<String>? = null

        if(DocumentsContract.isDocumentUri(this.requireContext(), uri)){
            if(isExternalStorageDocument(uri)){
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
            }
            else if(isDownloadsDocument(uri)){
                try {
                    val id = DocumentsContract.getDocumentId(uri)
                    uri = ContentUris.withAppendedId(
                        Uri.parse("content://dowloads/public_downloads"), java.lang.Long.valueOf(
                            id
                        )
                    )
                } catch (e: NumberFormatException){
                    return null
                }
            }
            else if (isMediaDocument(uri)){
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]
                if("image" == type){
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                }
                selection = "_id=?"
                selectionArgs = arrayOf(split[1])
            }

            if("content".equals(uri.scheme, ignoreCase = true)){
                val projection = arrayOf(MediaStore.Images.Media.DATA)

                var cursor: Cursor? = this.requireContext().contentResolver.query(
                    uri,
                    projection,
                    selection,
                    selectionArgs,
                    null
                )

                try {
                    if(cursor == null){
                        Log.d("Cursor", "Cursor is Empty")
                    }
                    else {

                        cursor.moveToFirst()
                        val colum_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                            Log.d("Cursor", "Cursor not Empty")
                            return cursor.getString(colum_index)
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }
            else if ("file".equals(uri.scheme, ignoreCase = true)){
                return uri.path
            }
            return null
        }

        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun hasExternalStorageWritePermission(): Boolean {
        return EasyPermissions.hasPermissions(
            this.requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        //Open gallery to pick image
        galleryIntent()
    }

    override fun onRationaleDenied(requestCode: Int) {
    }

    override fun onRationaleAccepted(requestCode: Int) {
    }

    override fun showProgressDialog() {
        //progressBar.visibility = View.VISIBLE
        //uploadButton.visibility = View.INVISIBLE
    }

    override fun hideProgressDialog() {
        //progressBar.visibility = View.INVISIBLE
        //uploadButton.visibility = View.VISIBLE
    }

    override fun onSuccess(imgUrl: String) {
        println("Uploaded File Path URL: $imgUrl")
    }

    override fun onError(errorMsg: String) {
        println("Uploaded File Path URL Error: $errorMsg")
    }

    fun successShowAlertDialog(){

        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Success")
            .setMessage("You Have Added a New Product!")
            .setPositiveButton("Okay"){ dialog, Which ->
                Log.e("Okay", "Okay")
            }
            .show()

    }

    fun failedShowAlertDialog(){

        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Alert")
            .setMessage("Something went wrong, please check your inputs or try again at a later time")
            .setPositiveButton("Okay"){ dialog, Which ->
                Log.e("Okau", "Okay")
            }
            .show()

    }
}