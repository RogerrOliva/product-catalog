package com.reavature.productcatalog.authmodel

//"username":"nobby",
//"password":"pass123",
//"email":"nob@yahoo.com","
//name":"Nob Nipsy",
//"phone":"9319439432",
//"address":"123 Nob Street"


data class BuyerProfileResponse(
    val cust_id: Int,
    val username: String,
    val password: String,
    val email: String,
    val name: String,
    val phone: String,
    val address: String,
    val message: String
)