package com.reavature.productcatalog.catalogmodel

data class CatalogResponse(
    val id:Int,
    val item: String,
    val description: String,
    val img0:String,
    val img1:String,
    val img2:String,
    val actual_price: Double,
    val selling_price:Double,
    val quantity: Double,
    val unit: String,
    val variants: String,
    val message:String
)
