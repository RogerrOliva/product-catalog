# Vendorise

## Project Description

This project is intended to allow small buisnesses to create an online store. They will be able to add items to a catalog and then market their catalog to their customers to keep them udapted on discounts, new products, and stock
## Technologies Used

* Kotlin - version 30.0
* Android Studio - version 4.1.3
* Web Services
* Retrofit
* Picasso
* GSON
* MVVM
* Material UI
* Jetpack Navigation
* Jetpack Safe Args
* Camera Intent
* Social Media Intent

## Features

List of features:
* Login and Register Functionality
* Business Profile
* Update Business Profile
* Add items to catalog
* View buisness catalog
* Product Details
* Delete Product 


## Getting Started
   
Git Clone: "git@gitlab.com:RogerrOliva/product-catalog.git"

Http Clone: https://gitlab.com/RogerrOliva/product-catalog.git

Instructions for Windows:
* Download files
* Open files in Android Studio 4.1.3 or later
* Run Project



## Usage

This project has some real world functionality. The buisness owner can add items to their catalog and distribute to customers.

## Contributors

* Rogerr Oliva
* Mathew Woods
* Uladzislau Shoka
* Ankit Patel

## License

This project uses the following license: [<license_name>](<link>).
